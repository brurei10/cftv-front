package br.com.orsegups.pocanalisecamerasapp.dto;

public class UsuarioFusionSimplesDTO {
	
	private String code;
	private int ramal;
	
	public int getRamal() {
		return ramal;
	}

	public void setRamal(int ramal) {
		this.ramal = ramal;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	@Override
	public String toString(){
		return "code="+code+";ramal="+ramal;
	}
	
}
