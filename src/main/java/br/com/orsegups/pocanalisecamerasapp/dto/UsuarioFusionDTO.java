package br.com.orsegups.pocanalisecamerasapp.dto;

import java.util.List;

public class UsuarioFusionDTO {
	
	Long id; 
	String code; 
	String name; 
	Long active; 
	String email; 
	Long idGroup; 
	Long cpf; 
	Long ramal; 
	Long idEscritorioRegional; 
	String nomeEscritorioRegional;
	
	List<PapelDTO> papeis;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getActive() {
		return active;
	}

	public void setActive(Long active) {
		this.active = active;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getIdGroup() {
		return idGroup;
	}

	public void setIdGroup(Long idGroup) {
		this.idGroup = idGroup;
	}

	public Long getCpf() {
		return cpf;
	}

	public void setCpf(Long cpf) {
		this.cpf = cpf;
	}

	public Long getRamal() {
		return ramal;
	}

	public void setRamal(Long ramal) {
		this.ramal = ramal;
	}

	public Long getIdEscritorioRegional() {
		return idEscritorioRegional;
	}

	public void setIdEscritorioRegional(Long idEscritorioRegional) {
		this.idEscritorioRegional = idEscritorioRegional;
	}

	public String getNomeEscritorioRegional() {
		return nomeEscritorioRegional;
	}

	public void setNomeEscritorioRegional(String nomeEscritorioRegional) {
		this.nomeEscritorioRegional = nomeEscritorioRegional;
	}

	public List<PapelDTO> getPapeis() {
		return papeis;
	}

	public void setPapeis(List<PapelDTO> papeis) {
		this.papeis = papeis;
	}

	@Override
	public String toString() {
		return "UsuarioFusionDTO [id=" + id + ", code=" + code + ", name=" + name + ", active=" + active + ", email="
				+ email + ", idGroup=" + idGroup + ", cpf=" + cpf + ", ramal=" + ramal + ", idEscritorioRegional="
				+ idEscritorioRegional + ", nomeEscritorioRegional=" + nomeEscritorioRegional + ", papeis=" + papeis
				+ "]";
	}
	
	

}
