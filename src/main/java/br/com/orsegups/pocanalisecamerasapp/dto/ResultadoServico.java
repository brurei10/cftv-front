package br.com.orsegups.pocanalisecamerasapp.dto;


public class ResultadoServico<T> {
	Long status;
	String msg;
	T ret;
	
	
	public ResultadoServico() {
		super();
	}


	public ResultadoServico(Long status, String msg, T ret) {
		super();
		this.status = status;
		this.msg = msg;
		this.ret = ret;
	}


	public Long getStatus()	{
		return status;
	}


	public void setStatus(Long status) {
		this.status = status;
	}


	public String getMsg() {
		return msg;
	}


	public void setMsg(String msg) {
		this.msg = msg;
	}


	public T getRet() {
		return ret;
	}


	public void setRet(T ret) {
		this.ret = ret;
	}


	@Override
	public String toString() {
		return "ResultadoServico [status=" + status + ", msg=" + msg + ", ret=" + ret + "]";
	}
	
}
