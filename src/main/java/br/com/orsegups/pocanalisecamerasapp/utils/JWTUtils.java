package br.com.orsegups.pocanalisecamerasapp.utils;

import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import br.com.orsegups.pocanalisecamerasapp.dto.UsuarioFusionSimplesDTO;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JWTUtils {

	@Inject
	private Environment env;

	/**
	 * Gera token de authenticaço CAS dinamicamente para comunicaçoes
	 * backend-backend
	 * 
	 * @param code
	 * @return
	 */
	public String generateToken(String code) {
		String key = env.getProperty("token.secret");
		int expiration = Integer.parseInt(env.getProperty("token.expiration.minutes"));

		String subject = code;
		String token = null;
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.MINUTE, expiration);

			token = Jwts.builder().setSubject(subject).signWith(SignatureAlgorithm.HS512, key).setIssuedAt(new Date())
					.setExpiration(calendar.getTime()).compact();

			return token;
		} catch (Exception e) {
			System.err.println("Erro ao gerar webtoken" + e.getMessage());
			return token;
		}

	}

	/**
	 * Retorna Headers de autenticaço para serem aplicados nas requisiçes via
	 * RestTemplate
	 * 
	 * @return
	 */
	public HttpEntity<String> getRequestEntity() {
		try {
			String token = generateToken("anonymous");

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set("X-AUTH-TOKEN", token);

			return new HttpEntity<String>(headers);

		} catch (Exception e) {
			System.err.println("Erro ao gerar RequestEntity" + e.getMessage());
			return null;
		}
	}

	/**
	 * Retorna Headers de autenticaço para serem aplicados nas requisiçes via
	 * RestTemplate <br>
	 * Retorna também o body contendo o usuário informado.
	 * 
	 * @param user
	 * @return
	 */
	public HttpEntity<String> getRequestEntity(UsuarioFusionSimplesDTO user) {
		try {
			String token = generateToken("anonymous");

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set("X-AUTH-TOKEN", token);

			Gson gson = new Gson();

			return new HttpEntity<String>(gson.toJson(user), headers);

		} catch (Exception e) {
			System.err.println("Erro ao gerar RequestEntity" + e.getMessage());
			return null;
		}
	}

}
