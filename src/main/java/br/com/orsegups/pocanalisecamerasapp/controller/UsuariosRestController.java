package br.com.orsegups.pocanalisecamerasapp.controller;

import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.orsegups.pocanalisecamerasapp.dao.UsuariosDAO;
import br.com.orsegups.pocanalisecamerasapp.dto.PapelDTO;
import br.com.orsegups.pocanalisecamerasapp.dto.ResultadoServico;
import br.com.orsegups.pocanalisecamerasapp.dto.UsuarioFusionDTO;

@RestController
@CrossOrigin
public class UsuariosRestController {


	@Autowired
	UsuariosDAO usuariosDAO;

	@GetMapping("/getInformacaoUsuario")
	@CrossOrigin
	@ResponseBody
	@Produces(MediaType.APPLICATION_JSON_VALUE)
	public ResultadoServico<UsuarioFusionDTO> getInformacaoUsuario(HttpServletRequest request) {

		UsuarioFusionDTO user = new UsuarioFusionDTO();
		try {
			Principal principal = request.getUserPrincipal();
			user = usuariosDAO.buscaUsuario(principal.getName());
			List<PapelDTO> papeis = usuariosDAO.listaPapeisUsuario(principal.getName());

			if (papeis != null) {
				user.setPapeis(papeis);
			}

			return new ResultadoServico<UsuarioFusionDTO>(200L, "OK", user);

		} catch (Exception e) {
			System.err.println("Erro ao buscar informações usuário. " + e.getMessage());
			return new ResultadoServico<UsuarioFusionDTO>(500L, "Erro no processamento do servico", null);
		}

	}

}
