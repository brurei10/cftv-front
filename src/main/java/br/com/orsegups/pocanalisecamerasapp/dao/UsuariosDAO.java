package br.com.orsegups.pocanalisecamerasapp.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import br.com.orsegups.pocanalisecamerasapp.dto.PapelDTO;
import br.com.orsegups.pocanalisecamerasapp.dto.ResultadoServico;
import br.com.orsegups.pocanalisecamerasapp.dto.UsuarioFusionDTO;
import br.com.orsegups.pocanalisecamerasapp.utils.JWTUtils;

@Component
public class UsuariosDAO {

	@Value("${service.consultaUsuarioFusion}")
	private String consultaUsuarioFusion;

	@Value("${service.consultaPapelUsuarioFusion}")
	private String consultaPapelUsuarioFusion;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private JWTUtils jwtUtils;

	public UsuarioFusionDTO buscaUsuario(String code) {
		UsuarioFusionDTO retorno = null;

		try {
			String url = consultaUsuarioFusion + code;
			ResultadoServico<UsuarioFusionDTO> response = restTemplate.exchange(url, HttpMethod.GET,
					jwtUtils.getRequestEntity(), new ParameterizedTypeReference<ResultadoServico<UsuarioFusionDTO>>() {
					}).getBody();

			retorno = response.getRet();

		} catch (Exception e) {
			System.err.println("Erro ao buscar usuário. " + e.getMessage());
		}
		return retorno;
	}

	public List<PapelDTO> listaPapeisUsuario(String code) {
		List<PapelDTO> retorno = null;

		try {
			String url = consultaPapelUsuarioFusion + code;
			ResultadoServico<List<PapelDTO>> response = restTemplate.exchange(url, HttpMethod.GET,
					jwtUtils.getRequestEntity(), new ParameterizedTypeReference<ResultadoServico<List<PapelDTO>>>() {
					}).getBody();

			retorno = response.getRet();

		} catch (Exception e) {
			System.err.println("Erro ao buscar papeis usuário. " + e.getMessage());
		}
		return retorno;
	}

}
