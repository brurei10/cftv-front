package br.com.orsegups.pocanalisecamerasapp.config;


import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * Servlet Filter implementation class for JWT generation
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class JWTGeneratorFilter implements Filter {
	
	@Value("${token.secret}")
	private String tokenSecret;

	@Value("${token.expiration.minutes}")
	private int tokenExpirationMinutes;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, tokenExpirationMinutes);

		String subject = "anonymouns";
		if (httpRequest.getUserPrincipal() != null) {
			subject = httpRequest.getUserPrincipal().getName();
		}

		String token = Jwts.builder()
				.setSubject(subject)
				.signWith(SignatureAlgorithm.HS512, tokenSecret)
				.setIssuedAt(new Date())
				.setExpiration(calendar.getTime())
				.compact();
		
		httpResponse.addHeader("X-AUTH-TOKEN", token);

		chain.doFilter(request, response);
	}
	
	@Override
	public void destroy() {}

}
