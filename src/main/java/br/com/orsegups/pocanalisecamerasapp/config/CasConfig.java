package br.com.orsegups.pocanalisecamerasapp.config;

import java.io.IOException;
import java.util.Arrays;
import java.util.UUID;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class CasConfig {

    private static Logger logger = LoggerFactory.getLogger(CasConfig.class);

    @Autowired
    Environment environment;

    @Bean
    public FilterRegistrationBean singleSignOutFilter() {
        FilterRegistrationBean singleSignOutWrapper = new FilterRegistrationBean();
        LogoutFilter filter = new LogoutFilter();
        singleSignOutWrapper.setFilter(filter);
        singleSignOutWrapper.setOrder(4);
        singleSignOutWrapper.setUrlPatterns(Arrays.asList("/*"));
        return singleSignOutWrapper;
    }

    class LogoutFilter implements Filter {

        @Override
        public void init(FilterConfig filterConfig) throws ServletException {}

        @Override
        public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                             FilterChain filterChain) throws IOException, ServletException {
            HttpServletRequest request = (HttpServletRequest) servletRequest;
            HttpServletResponse response = (HttpServletResponse) servletResponse;

            logger.info(">>> " +request.getServletPath());

            if (request.getServletPath().equals("/logout")) {
                request.getSession(false).invalidate();

                response.setHeader("Cache-Control","no-store"); //HTTP 1.1
                response.setHeader("Pragma","no-cache"); //HTTP 1.0
                response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
                response.sendRedirect(environment.getProperty("cas.client-host-url") + "?uuid=" + UUID.randomUUID());
            } else {
                filterChain.doFilter(request, response);
            }
        }

        @Override
        public void destroy() {}

    }
}
