'use strict';
angular.module('cftvInteligente').factory("tarefasAPI", function ($http, SERVICES) {

    var _getTarefa = function (operador) {

        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'getTarefa',
                params: { operador: operador }
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao buscar tarefas!');
                return reject(response.data);
            });
        });

    };

     var _filtraErroConexaoServidor = function (operador) {

        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'filtraErroConexaoServidor',
               
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao buscar tarefas!');
                return reject(response.data);
            });
        });

    };

var _filtraLabelsNaoSatisfazem = function (operador) {

        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'filtraLabelsNaoSatisfazem',
               
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao buscar tarefas!');
                return reject(response.data);
            });
        });

    };

    var _filtraCameraSem = function (operador) {

        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'filtraCameraSem',
               
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao buscar tarefas!');
                return reject(response.data);
            });
        });

    };

    var _filtraNaoPossivelObter = function (operador) {

        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'filtraNaoPossivelObter',
               
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao buscar tarefas!');
                return reject(response.data);
            });
        });

    };
    

    var _getTarefaAnterior = function (idTarefaAnterior, idTarefaAtual, operador) {

        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'getTarefaAnterior',
                params: { idTarefaAnterior: idTarefaAnterior, idTarefaAtual: idTarefaAtual, operador: operador }
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao buscar tarefa anterior!');
                return reject(response.data);
            });
        });

    };

    var _getProximaTarefa = function (idTarefaAtual, operador) {

        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'getProximaTarefa',
                params: { idTarefaAtual: idTarefaAtual, operador: operador }
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao buscar proxima tarefa !');
                return reject(response.data);
            });
        });

    };

    var _getProximaTarefaFiltrada = function (idTarefaAtual, operador,idTarefaFiltro) {

        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'getProximaTarefaFiltrada',
                params: { idTarefaAtual: idTarefaAtual, operador: operador ,idTarefaFiltro: idTarefaFiltro}
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao buscar proxima tarefa !');
                return reject(response.data);
            });
        });

    };

    var _getTarefaFiltro = function (idTarefaFiltro, idTarefaAtual, operador) {

        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'getTarefaFiltro',
                params: { idTarefaFiltro: idTarefaFiltro, idTarefaAtual: idTarefaAtual, operador: operador }
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao buscar tarefa do filtro!');
                return reject(response.data);
            });
        });

    };

     var _getTarefaFiltroHistory = function (idTarefaFiltro, idTarefaAtual, operador) {

        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'getTarefaFiltroHistory',
                params: { idTarefaFiltro: idTarefaFiltro, idTarefaAtual: idTarefaAtual, operador: operador }
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao buscar tarefa do filtro!');
                return reject(response.data);
            });
        });

    };

    var _getQuantidadeTarefasAbertas = function () {
        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'getQuantidadeTarefasAbertas'
               
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao retornar quantidade de tarefas abertas!');
                return reject(response.data);
            });
        });
    };

    var _getQuantidadeTarefasNoReferencias = function () {
        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'getQuantidadeTarefasNoReferencias'
               
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao retornar quantidade de tarefas abertas!');
                return reject(response.data);
            });
        });
    };

     var _getQuantidadeTarefasErroServidor = function () {
        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'getQuantidadeTarefasErroServidor'
               
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao retornar quantidade de tarefas abertas!');
                return reject(response.data);
            });
        });
    };

     var _getQuantidadeTarefasNaoPossivelObterImagem = function () {
        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'getQuantidadeTarefasNaoPossivelObterImagem'
               
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao retornar quantidade de tarefas abertas!');
                return reject(response.data);
            });
        });
    };

     var _getQuantidadeTarefasLabelsNao = function () {
        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'getQuantidadeTarefasLabelsNao'
               
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao retornar quantidade de tarefas abertas!');
                return reject(response.data);
            });
        });
    };

     var _getQuantidadeTarefasImagemReprocessada = function () {
        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES +  'getQuantidadeTarefasImagemReprocessada'
               
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao retornar quantidade de tarefas abertas!');
                return reject(response.data);
            });
        });
    };

    var _getQuantidadeOperadoresAtivos = function () {
        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'getQuantidadeOperadoresAtivos'

            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao retornar quantidade de operadores ativos!');
                return reject(response.data);
            });
        });
    };

    var _listarTarefasAbertas = function () {
        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'listarTarefasAbertas',
                              
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao retornar quantidade de tarefas abertas!');
                return reject(response.data);
            });
        });
    };

var _listarTarefasAbertasAll = function () {
        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'listarTarefasAbertasAll',
                              
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao retornar quantidade de tarefas abertas!');
                return reject(response.data);
            });
        });
    };

    var _getOperadoresAtivos = function () {
        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'getOperadoresAtivos',
                              
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao retornar quantidade de operadores ativos!');
                return reject(response.data);
            });
        });
    };




    var _salvarTarefa = function (tarefa) {
        return new Promise(function (resolve, reject) {
            $http({
                method: 'POST',
                url: SERVICES + 'salvarTarefa',
                data: tarefa
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao salvar tarefa!');
                return reject(response.data);
            });
        });
    };

	
    
     var _updateTesteAutomatico = function (cliente) {

        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'updateTesteAutomatico',
                params: {cliente: cliente}
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao buscar tarefa do filtro!');
                return reject(response.data);
            });
        });

    };

    var _enviarRat = function (tarefa) {
        return new Promise(function (resolve, reject) {
            $http({
                method: 'POST',
                url: SERVICES + 'enviarRat',
                data: tarefa
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao enviar rat tarefa!');
                return reject(response.data);
            });
        });
    };

     var _getCallUra= function (ramal,fone1) {
        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'getCallUra',
                 params: { ramal: ramal, fone1: fone1 }
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao enviar rat tarefa!');
                return reject(response.data);
            });
        });
    };

    var _getDadosCentralAlarme = function (cdCliente) {
        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'getDadosCentralAlarme',
                 params: { cdCliente: cdCliente }
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao atualizar status do alarme!');
                return reject(response.data);
            });
        });
    };


    

     var _getRatsEnviadas = function (cpfCnpj) {
        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: 'https://mqs.orsegups.com.br/mail_history/find_by_service?cpfCnpj='+cpfCnpj+'&serviceCode=CFTV_INTELIGENTE&page=0&size=5',
                data: cpfCnpj
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
              //  console.log('Erro ao recuperar histórico rats!');
                return reject(response.data);
            });
        });
    };

    var _historicoTarefas = function(id) {
        return new Promise(function(resolve, reject) {
            $http({
                method: 'POST',
                url: SERVICES + 'getHistorico',
                params: {id: id}
            }).then(function successCallback(response) {
               
                resolve(response.data);
            }, function errorCallback(response) {
               
                console.log('Erro ao carregar historico Tarefas!');
                reject(response.data);
            });
        });
    };





    return {
        historicoTarefas: _historicoTarefas,
        getDadosCentralAlarme: _getDadosCentralAlarme,
    	getCallUra: _getCallUra,
    	getRatsEnviadas: _getRatsEnviadas,
        salvarTarefa: _salvarTarefa,
        enviarRat: _enviarRat,
        getTarefa: _getTarefa,
        getQuantidadeTarefasAbertas: _getQuantidadeTarefasAbertas,
        getQuantidadeOperadoresAtivos:_getQuantidadeOperadoresAtivos,
        getTarefaAnterior: _getTarefaAnterior,
        getProximaTarefa: _getProximaTarefa,
        listarTarefasAbertas: _listarTarefasAbertas,
        listarTarefasAbertasAll: _listarTarefasAbertasAll,
        getTarefaFiltro: _getTarefaFiltro,
        getTarefaFiltroHistory: _getTarefaFiltroHistory,
        getQuantidadeTarefasErroServidor: _getQuantidadeTarefasErroServidor,
        getQuantidadeTarefasNoReferencias: _getQuantidadeTarefasNoReferencias,
        getQuantidadeTarefasNaoPossivelObterImagem: _getQuantidadeTarefasNaoPossivelObterImagem,
        getQuantidadeTarefasLabelsNao: _getQuantidadeTarefasLabelsNao,
        getQuantidadeTarefasImagemReprocessada: _getQuantidadeTarefasImagemReprocessada,
        getOperadoresAtivos:_getOperadoresAtivos,
        filtraErroConexaoServidor:_filtraErroConexaoServidor,
        filtraLabelsNaoSatisfazem:_filtraLabelsNaoSatisfazem,
        filtraCameraSem:_filtraCameraSem,
        filtraNaoPossivelObter:_filtraNaoPossivelObter,
        getProximaTarefaFiltrada:_getProximaTarefaFiltrada,
        updateTesteAutomatico:_updateTesteAutomatico,
    };

});