'use strict';
angular.module('cftvInteligente').factory("usuariosAPI", function($http, WEB_BASE_URL) {


    var _getInformacaoUsuario = function() {

        return new Promise(function(resolve, reject) {
            $http({
                method: 'GET',
                url: WEB_BASE_URL + 'getInformacaoUsuario'
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao buscar informações usuário!');
                return reject(response.data);
            });
        });
    }


    return {
        getInformacaoUsuario: _getInformacaoUsuario
    };

});