'use strict';
angular.module('cftvInteligente').factory("ordemServicoAPI", function($http, SERVICES) {

    var _salvarOS = function(os) {
        return new Promise(function(resolve, reject) {
            $http({
                method: 'POST',
                url: SERVICES + 'salvarOS',
                data: os
            }).then(function successCallback(response) {
                $('#myModal').modal('hide');
                resolve(response.data);
            }, function errorCallback(response) {
                $('#myModal').modal('hide');
                console.log('Erro ao cadastrar OS!');
                reject(response.data);
            });
        });
    };

    var _historicoOS = function(tarefas) {
        return new Promise(function(resolve, reject) {
            $http({
                method: 'POST',
                url: SERVICES + 'historicoOS',
                data: tarefas
            }).then(function successCallback(response) {
               
                resolve(response.data);
            }, function errorCallback(response) {
               
                console.log('Erro ao carregar historico OS!');
                reject(response.data);
            });
        });
    };

    return {
        salvarOS: _salvarOS,
        historicoOS: _historicoOS
    };

});