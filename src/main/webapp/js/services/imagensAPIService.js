'use strict';
angular.module('cftvInteligente').factory("imagensAPI", function ($http, SERVICES,cfpLoadingBar) {

  var start = function () {
        cfpLoadingBar.start();
    };

    
	
    var _getImagemAoVivo = function (tarefa, imagem, verificarLabels) {
	
        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                url: SERVICES + 'getImagemAoVivo/' + tarefa.cliente.cdCliente + '/' + imagem.camera.id + '/' + tarefa.cliente.servidorCftv + '/' + tarefa.cliente.idEmpresa + '/' + verificarLabels + '/' + imagem.id
            }).then(function successCallback(response) {
			
                resolve(response);
            }, function errorCallback(response) {
		
                console.log("Erro ao buscar imagem ao vivo.");
                reject(response);
            });
        });
    };

    var _buscarReferencias = function (camera) {
        return new Promise(function (resolve, reject) {
            $http({
                method: 'POST',
                url: SERVICES + 'buscarReferencias',
                data: camera
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao buscar referencias!');
                return reject(response.data);
            });
        });
    };

    var _getSessionId = function (servidor) {
        return new Promise(function (resolve, reject) {
            var url = servidor;
            $http({
                method: 'GET',
                headers: {
                    'Authorization': 'Basic ' + btoa('sigma:sigma')
                },
                responseType: 'arraybuffer',
                url: url
            }).then(function successCallback(response) {
                var retorno = response.headers('SessaoId');
                resolve(retorno);
            }, function errorCallback(response) {
                console.log('Erro ao buscar sessionId!');
                reject(response);
            });
        });
    };

    var _getImagem = function (url, camera, sessionId) {
        var retorno = {};
        retorno.id = camera;

        return new Promise(function (resolve, reject) {
            $http({
                method: 'GET',
                headers: {
                    'Authorization': 'Basic ' + btoa('sigma:sigma'),
                    'SessaoId': sessionId
                },
                ignoreLoadingBar: true,
                timeout: 3000,
                responseType: 'arraybuffer',
                url: url
            }).then(function successCallback(response) {
                retorno.src = arrayBufferToBase64(response.data);
                return resolve(retorno);
            }, function errorCallback(response) {
                retorno.src = 'loadFail';
                return reject(retorno);
            });
        });
    };


    var _deletarReferencia = function (imagemReferencia) {
        return new Promise(function (resolve, reject) {
            $http({
                method: 'POST',
                url: SERVICES + 'deletarReferencia',
                data: imagemReferencia
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao deletar referencia!');
                return reject(response.data);
            });
        });
    };
	
	var _deletarTodasReferencias = function (slidesReferencias) {
        return new Promise(function (resolve, reject) {
            $http({
                method: 'POST',
                url: SERVICES + 'deletarTodasReferencias',
                data: slidesReferencias
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao deletar referencias!');
                return reject(response.data);
            });
        });
    };


    var _salvarImagem = function (imagem) {
        return new Promise(function (resolve, reject) {
            $http({
                method: 'POST',
                url: SERVICES + 'salvarImagem',
                data: imagem
            }).then(function successCallback(response) {
                return resolve(response.data);
            }, function errorCallback(response) {
                console.log('Erro ao salvar imagem!');
                return reject(response.data);
            });
        });
    };

    var arrayBufferToBase64 = function (buffer) {
        var binary = '';
        var bytes = new Uint8Array(buffer);
        var len = bytes.byteLength;
        for (var i = 0; i < len; i++) {
            binary += String.fromCharCode(bytes[i]);
        }
        return window.btoa(binary);
    };

    
    return {
        getSessionId: _getSessionId,
        getImagem: _getImagem,
        getImagemAoVivo: _getImagemAoVivo,
        buscarReferencias: _buscarReferencias,
        deletarReferencia: _deletarReferencia,
		deletarTodasReferencias: _deletarTodasReferencias,
        salvarImagem: _salvarImagem
    };

});