angular.module('cftvInteligente').config(function($locationProvider, $httpProvider) {
    $httpProvider.interceptors.push('errorHandlerInterceptor');
    $httpProvider.interceptors.push('authInterceptor');
    $locationProvider.hashPrefix('');
});