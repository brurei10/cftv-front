angular.module('cftvInteligente').config(function($routeProvider) {

    $routeProvider.when("/tarefa/:uuId", {
        templateUrl: "view/tarefa.html",
        controller: "tarefaCtrl"
    });

    $routeProvider.when("/tarefa", {
        templateUrl: "view/tarefa.html",
        controller: "tarefaCtrl"
    });

    $routeProvider.when("/", {
        templateUrl: "view/tarefa.html",
        controller: "tarefaCtrl"
    });
    
});