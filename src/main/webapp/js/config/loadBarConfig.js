angular.module('cftvInteligente')
    .config(function(cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeSpinner = true;
        cfpLoadingBarProvider.includeBar = true;
        cfpLoadingBarProvider.spinnerTemplate = '<div id="loading-bar-spinner" class="panel panel-default"><div class="panel-body"><img id="imgSpinner" src="./images/loader.gif"/><p>Processando Imagens...</p></div></div>';
    });