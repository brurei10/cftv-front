'use strict';
angular.module('cftvInteligente').controller('tarefaCtrl', function ($scope, $rootScope, tarefasAPI, ordemServicoAPI, usuariosAPI, imagensAPI, cfpLoadingBar, $timeout) {
    $rootScope.operador;
    $rootScope.ramal;

    $scope.tarefasMemoria = [];
    $scope.tarefasFiltro = [];
    $scope.tarefasFiltroAll = [];
    $scope.slides = [];
    $scope.slidesReferencias = [];
    $scope.labelsImagemCamera = [{ 'descricao': '', 'confidence': '', 'match': false }];
    $scope.labelsImagemReferencia = [{ 'descricao': '', 'confidence': '', 'match': false }];
    $scope.disableBtnSalvar = false;

    $scope.indexOsSelecionado = 0;
    $scope.tarefa = undefined;
    $scope.historicoOs = [];
    $scope.historicoTarefas = [];
    $scope.historicoRats = [];
    $scope.countDefeito = 0;
    $scope.countTotal = 0;
    $scope.countTotalCamerasTratar = 0;
    $scope.slideSelecionado = {};
    $scope.labelsref = {};
    $scope.os = {};
    $scope.formulario = { 'acao': "", 'justificativa': '' };
    $scope.indexRat = undefined;

    $scope.checkedSomenteCamerasDefeito = true;
    $scope.checkedEnviaRat = false;
    $scope.showInfoCliente = false;
    $scope.loading = true;
    $scope.showForm = false;
    $scope.showBotoes = false;
    $scope.imgAoVivo = undefined;

    $scope.msgErro = undefined;
    $scope.msgSucesso = undefined;
    $scope.qtdTarefas = '';
    $scope.qtdOperadores = '';
    $scope.msgCameras = "Carregando Câmeras com defeito";
    $scope.msgCamerasReferencia = "Câmera não possui imagens referência.";
    var xhr = new XMLHttpRequest();
    $scope.porcentagemMatchImagem = 0;
    $scope.porcentagemMatchImagemRef = 0;
    $scope.flagDefeito = 0;
    $scope.flagTarefa = false;
    $scope.totalReferenciasCurrent = 0;

    $scope.qtdTarefasErroServidor = 0;
    $scope.qtdTarefasNoReferencias = 0;
    $scope.qtdTarefasNaoPossivelObterImagem = 0;
    $scope.qtdTarefasLabelsNao = 0;
    $scope.qtdTarefasImagemReprocessada = 0;
    
    var validaFone = "";
    var validaFone1= "";
    var validaFone2 = "";
    var validaFoneCel = "";
    
    $scope.formated = "";
    $scope.operadoresAtivos = [];
    
    var validaAlert = false;
    
    $scope.selectedValue ="1";
    $scope.data = new Date();
    
    var dia = $scope.data.getDate();
    var mes = $scope.data.getMonth();
    var ano4 = $scope.data.getFullYear();

    var hora =  $scope.data.getHours();          // 0-23
    var min =  $scope.data.getMinutes();        // 0-59
    var seg =  $scope.data.getSeconds();        // 0-59

    var onInit = function () {
    	
        getQuantidadeTarefasAbertas();
        getOperadoresAtivos();
        $scope.qtdOperadores= getQuantidadeOperadoresAtivos();
        getInformacaoUsuario();
        listarTarefasFiltro();
        listarTarefasFiltroAll();
        update();
        $("#rat").prop('disabled', false);
        $(document).ready(function() {
        $('#example').DataTable();
     });
      
    };

   $scope.criapdf =  function (lista) {
	   
        var minhaTabela = document.getElementById(lista).innerHTML;
        var style = "<style>";
        style = style + "table {width: 100%;font: 20px Calibri;}";
        style = style + "table, th, td {border: solid 1px #DDD; border-collapse: collapse;";
        style = style + "padding: 2px 3px;text-align: center;}";
        style = style + "</style>";
        // CRIA UM OBJETO WINDOW
        var win = window.open('', '', 'height=700,width=700');
        win.document.write('<html><head>');
        win.document.write('<title>Historico </title>');   
        win.document.write(style);                                    
	    win.document.write('</head>');
        win.document.write('<body>');
        win.document.write(minhaTabela);                         																														
        win.document.write('</body></html>');
        win.document.close();                                          														
        win.print();                                                    
   }
   
    var update = function () {
       
         getQuantidadeTarefasAbertas();
         $scope.qtdOperadores= getQuantidadeOperadoresAtivos();
         getOperadoresAtivos();
         
         $timeout(function () {
           update();
               
         }, 30000);

    }

     $scope.clickFone = function () {
        if(validaFone==0){
             setMsgErro("Não identificamos seu ramal. Contacte o suporte.");
        }else if(validaFone1 ==null || validaFone1 == undefined){
             setMsgErro("Não identificamos o Telefone 1 deste cliente. Contacte o suporte.");
        } else if(validaFone==="" ){
          
        }
        else{
             $scope.tarefa.cliente.fone1 = $scope.tarefa.cliente.fone1.replace(/[^0-9]/g, '');
            // $scope.getCallUra();
             event.keyCode = 9;     
             open('http://192.168.20.241/snep/services/?service=Dial&ramal='+$rootScope.ramal+'&exten='+$scope.tarefa.cliente.fone1);
             focus();
            // setTimeout (window.close, 5000);
             alert("Fique atento ao Ramal "+$rootScope.ramal+" Uma ligação de confirmação será iniciada. DISCANDO....");
                    
        	}
       
       }

          $scope.getCallUra =  function(){
             start();
                tarefasAPI.getCallUra($rootScope.ramal,$scope.tarefa.cliente.fone1)
                    .then(function (response) {
                       
                        complete();
                                           
                    }, function errorCallback(response) {
                        complete();
                        console.log("Erro ao discar para o cliente.");
                        console.log(response);
                    });

         }

           $scope.getCallUraFone2 =  function(){
             start();
                tarefasAPI.getCallUra($rootScope.ramal,$scope.tarefa.cliente.fone2)
                    .then(function (response) {
                       
                        complete();
                                           
                    }, function errorCallback(response) {
                        complete();
                        console.log("Erro ao discar para o cliente.");
                        console.log(response);
                    });

         }

          $scope.getCallUraCelular =  function(){
             start();
                tarefasAPI.getCallUra($rootScope.ramal,$scope.tarefa.cliente.celular)
                    .then(function (response) {
                       
                        complete();
                                           
                    }, function errorCallback(response) {
                        complete();
                        console.log("Erro ao discar para o cliente.");
                        console.log(response);
                    });

         }

         $scope.getDadosCentralAlarme =  function(){
           
                  start();
                 limparCameraAoVivo();
                tarefasAPI.getDadosCentralAlarme($scope.tarefa.cliente.cdCliente)
                    .then(function (response) {
                       $scope.tarefa.logProcessamentos = "Status Alarme atualizado nesta tarefa. ANTES - "+$scope.tarefa.cliente.statusAlarme+" DEPOIS " +response;
                       $scope.tarefa.cliente.statusAlarme =  response;
                       $scope.tarefa.statusAlarme = response;
                       $scope.tarefa.logProcessamentos = "Status Alarme atualizado nesta tarefa. Anterior"
                        complete();
                           
                    }, function errorCallback(response) {
                        complete();
                       
                        console.log(response);
                    });

         }

   $scope.clickFone2 = function () {
             
	  if(validaFone==0){
           setMsgErro("Não identificamos seu ramal. Contacte o suporte.");
      }else if(validaFone2 ==null || validaFone2 == undefined){
           setMsgErro("Não identificamos o Telefone 2 deste cliente. Contacte o suporte.");
      }else if(validaFone==="" ){
           
      }
        else{
             $scope.tarefa.cliente.fone2 = $scope.tarefa.cliente.fone2.replace(/[^0-9]/g, '');
             // $scope.getCallUraFone2();
              open('http://192.168.20.241/snep/services/?service=Dial&ramal='+$rootScope.ramal+'&exten='+$scope.tarefa.cliente.fone2);
             focus();
        
            alert("Fique atento ao Ramal "+$rootScope.ramal+" Uma ligação de confirmação será iniciada. DISCANDO....");
    
       }
   
   }

   $scope.clickFoneCel = function () {
        
	  if(validaFone==0){
           setMsgErro("Não identificamos seu Ramal. Contacte o suporte.");
      }else if(validaFoneCel ==null || validaFoneCel == undefined){
           setMsgErro("Não identificamos o Celular deste cliente. Contacte o suporte.");
      }else if(validaFone==="" ){
           
      }
        else{
             $scope.tarefa.cliente.celular = $scope.tarefa.cliente.celular.replace(/[^0-9]/g, '');
            $scope.getCallUraCelular();
        
            alert("Fique atento ao Ramal "+$rootScope.ramal+" Uma ligação de confirmação será iniciada. DISCANDO....");
    
       }
   
   }

    var getInformacaoUsuario = function () {
        start();
        usuariosAPI.getInformacaoUsuario()
            .then(function (response) {
                $rootScope.operador = response.ret.code;
                $rootScope.ramal = response.ret.ramal;
                validaFone =  $rootScope.ramal;
               
                complete();
                getTarefa();
               
            }, function errorCallback(response) {
                complete();
                console.log("Erro ao buscar informações usuário.");
                console.log(response);
            });
    };

    var setMsgErro = function (msg) {
        $scope.msgErro = msg;
        $timeout(function () {
            $scope.msgErro = undefined;
        }, 3000);
    };

    var setMsgSucesso = function (msg) {
        $scope.msgSucesso = msg;
        $timeout(function () {
            $scope.msgSucesso = undefined;
        }, 4000);
    };

    var getTarefaAnterior = function (idTarefaAnterior, idTarefaAtual, operador) {
        start();
        tarefasAPI.getTarefaAnterior(idTarefaAnterior, idTarefaAtual, operador).then(function (response) {

            $scope.slides = [];
            limparLabelsImagem();
            limparReferencias();
            $scope.tarefa = {};

            if (response != null && response !== '') {
                $scope.tarefa = response;
                $scope.slides = $scope.tarefa.listImagens;
                $scope.msgCameras = "";
                $scope.msgCamerasReferencia = "";
                console.log("getTarefaAnterior");
                filtrarCheckbox();

            } else {
                $scope.msgCameras = "Nenhuma tarefa disponível no momento!";
                $scope.msgCamerasReferencia = "Nenhuma tarefa disponível no momento!";
                setMsgErro("Tarefa não está disponível!");
            }
            complete();
        }, function errorCallback(response) {
            complete();
            setMsgErro("Erro ao buscar tarefa!");
            console.log("Erro ao buscar tarefa! " + response);
        });
    };

    var getProximaTarefa = function (idTarefaAtual, operador) {
        start();

        if (idTarefaAtual === undefined) {
            idTarefaAtual = 0;
        }

        tarefasAPI.getProximaTarefa(idTarefaAtual, operador).then(function (response) {

            $scope.slides = [];
            limparLabelsImagem();
            limparReferencias();
            $scope.tarefa = {};

            if (response != null && response !== "") {
                $scope.tarefasMemoria.push(response.id);
                $scope.tarefa = response;
                $scope.slides = $scope.tarefa.listImagens;
                $scope.msgCameras = "";
                $scope.msgCamerasReferencia = "";
                filtrarCheckbox();
                getHistoricoOS();
                getHistoricoTarefas();
                getRatsEnviadas();
            } else {
                $scope.msgCameras = "Nenhuma tarefa disponível no momento!";
                $scope.msgCamerasReferencia = "Nenhuma tarefa disponível no momento!";
                setMsgErro("Nenhuma tarefa disponível no momento!");
            }
            complete();
        }, function errorCallback(response) {
            complete();
            setMsgErro("Erro ao buscar tarefa!");
            console.log("Erro ao buscar tarefa! " + response);
        });
    };

    var getProximaTarefaFiltrada = function (idTarefaAtual, operador,idFiltro) {
        start();

        if (idTarefaAtual === undefined) {
            idTarefaAtual = 0;
        }

        tarefasAPI.getProximaTarefaFiltrada(idTarefaAtual, operador,idFiltro).then(function (response) {

            $scope.slides = [];
            limparLabelsImagem();
            limparReferencias();
            $scope.tarefa = {};

            if (response != null && response !== "") {
                $scope.tarefasMemoria.push(response.id);
                $scope.tarefa = response;
                $scope.slides = $scope.tarefa.listImagens;
                $scope.msgCameras = "";
                $scope.msgCamerasReferencia = "";
                filtrarCheckbox();
                getHistoricoOS();
                getHistoricoTarefas();
                getRatsEnviadas();

            } else {
                $scope.msgCameras = "Nenhuma tarefa disponível no momento!";
                $scope.msgCamerasReferencia = "Nenhuma tarefa disponível no momento!";
                setMsgErro("Nenhuma tarefa disponível no momento!");
            }
            complete();
        }, function errorCallback(response) {
            complete();
            setMsgErro("Erro ao buscar tarefa!");
            console.log("Erro ao buscar tarefa! " + response);
        });
    };



    var getTarefa = function () {
        start();
        tarefasAPI.getTarefa($rootScope.operador).then(function (response) {

            $scope.slides = [];
            limparLabelsImagem();
            limparReferencias();
            $scope.tarefa = {};

            if (response != null && response !== '') {
                $scope.tarefasMemoria.push(response.id);
                $scope.tarefa = response;
                $scope.slides = $scope.tarefa.listImagens;
                $scope.msgCameras = "";
                $scope.msgCamerasReferencia = "";
                filtrarCheckbox();
                getHistoricoOS();
                getHistoricoTarefas();
                getRatsEnviadas();
                validaFoneCel = $scope.tarefa.cliente.celular;
                validaFone1 = $scope.tarefa.cliente.fone1;
                validaFone2 = $scope.tarefa.cliente.fone2;
            } else {
                $scope.msgCameras = "Nenhuma tarefa disponível no momento!";
                $scope.msgCamerasReferencia = "Nenhuma tarefa disponível no momento!";
            }
            complete();
        }, function errorCallback(response) {
            complete();
            console.log("Erro ao buscar tarefa! " + response);
            
              try{
                	
                if($scope.tarefa.cliente !== null && $scope.tarefa.cliente !== undefined){
                    $scope.proximaTarefa();
                }else{
                    $scope.proximaTarefa();
                      }

               }catch{
                  $scope.proximaTarefa();

               }
        });
    };

    var listarTarefasFiltro = function () {
        desabilitarFiltro();
        tarefasAPI.listarTarefasAbertas().then(function (response) {

            if (response !== undefined && response.length > 0) {

                response.forEach(tarefa => {
                    if (tarefa !== undefined) {
                        tarefa.ultimoProcessamento = new Date(tarefa.ultimoProcessamento).toLocaleDateString();
                    }
                });

                $scope.tarefasFiltro = response;
                $scope.tarefasFiltro.push($scope.tarefa);
                habilitarFiltro();
            }

        }, function errorCallback(response) {
            habilitarFiltro();
           
            console.log("Erro ao listar tarefas abertas! " + response);

            try{
                if($scope.tarefa.cliente !== null && $scope.tarefa.cliente !== undefined ){
                                
                }
                else{
                   $scope.proximaTarefa();
                    }

                }catch{
                    $scope.proximaTarefa();

                }
   
        });
    };

    var listarTarefasFiltroAll = function () {
        desabilitarFiltro();
        tarefasAPI.listarTarefasAbertasAll().then(function (response) {

            if (response !== undefined && response.length > 0) {

                response.forEach(tarefa => {
                    if (tarefa !== undefined) {
                        tarefa.ultimoProcessamento = new Date(tarefa.ultimoProcessamento).toLocaleDateString();
                    }
                });

                $scope.tarefasFiltroAll = response;
                $scope.tarefasFiltroAll.push($scope.tarefa);
                habilitarFiltro();
            }

        }, function errorCallback(response) {
            habilitarFiltro();
           
            console.log("Erro ao listar tarefas abertas! " + response);

                try{
                if($scope.tarefa.cliente !== null && $scope.tarefa.cliente !== undefined ){
                                
                    }else{
                       $scope.proximaTarefa();
                   }
 
                }catch{
                    $scope.proximaTarefa();

                }
  
        });
    };
    
    var getOperadoresAtivos = function () {
      
        tarefasAPI.getOperadoresAtivos().then(function (response) {

            if (response !== undefined && response.length > 0) {
                
                $scope.operadoresAtivos = response;
              
            }

        }, function errorCallback(response) {
          
             
        });
    };

    $scope.getSelectValueProximo = function () {
    	
            console.log("testando");
            // var selectedText = $(this).find("option:selected").text();
            $scope.selectedValue = $("#comboFiltro").val();

            if($scope.selectedValue==1 &&  $scope.slides.length<=0){
             
            	$scope.proximaTarefa();

            }else if($scope.selectedValue==2 && $scope.slides.length<=0){

                if( $scope.qtdTarefasLabelsNao.length!=0){

                
                	tarefasAPI.filtraLabelsNaoSatisfazem().then(function (response) {

                        if (response !== undefined && response.length > 0) {

                            response.forEach(tarefa => {
                                if (tarefa !== undefined) {
                                    tarefa.ultimoProcessamento = new Date(tarefa.ultimoProcessamento).toLocaleDateString();
                                }
                            });

                            $scope.tarefasFiltro = response;
                            $scope.tarefasFiltro.push($scope.tarefa);
                            habilitarFiltro();
                        }
                         $scope.proximaTarefaFiltrada();

                    }, function errorCallback(response) {
                        habilitarFiltro();
                       
                        console.log("Erro ao listar tarefas abertas! " + response);

                        try{
                            if($scope.tarefa.cliente !== null && $scope.tarefa.cliente !== undefined ){
                                            
                              }else{
                                $scope.proximaTarefaFiltrada();
                                  }

                            }catch{
                                $scope.proximaTarefaFiltrada();

                            }

                    });
               }

            }
            else if($scope.selectedValue==3 && $scope.slides.length<=0){

                if( $scope.qtdTarefasErroServidor.length!=0){
                 tarefasAPI.filtraErroConexaoServidor().then(function (response) {

                        if (response !== undefined && response.length > 0) {

                            response.forEach(tarefa => {
                                if (tarefa !== undefined) {
                                    tarefa.ultimoProcessamento = new Date(tarefa.ultimoProcessamento).toLocaleDateString();
                                }
                            });

                            $scope.tarefasFiltro = response;
                            $scope.tarefasFiltro.push($scope.tarefa);
                            habilitarFiltro();
                        }
                         $scope.proximaTarefaFiltrada();

                    }, function errorCallback(response) {
                        habilitarFiltro();
                       
                        console.log("Erro ao listar tarefas abertas! " + response);

                            try{
                            if($scope.tarefa.cliente !== null && $scope.tarefa.cliente !== undefined ){
                                            
                             }else{
                               $scope.proximaTarefaFiltrada();
                                  }

                            }catch{
                                $scope.proximaTarefaFiltrada();

                            }

                    });

             }
                
            }else if ($scope.selectedValue==4 && $scope.slides.length<=0){

                if( $scope.qtdTarefasNoReferencias.length!=0){
                	
                	tarefasAPI.filtraCameraSem().then(function (response) {

                        if (response !== undefined && response.length > 0) {

                            response.forEach(tarefa => {
                                if (tarefa !== undefined) {
                                    tarefa.ultimoProcessamento = new Date(tarefa.ultimoProcessamento).toLocaleDateString();
                                }
                            });

                            $scope.tarefasFiltro = response;
                            $scope.tarefasFiltro.push($scope.tarefa);
                            habilitarFiltro();
                        }
                         $scope.proximaTarefaFiltrada();

                    }, function errorCallback(response) {
                        habilitarFiltro();
                       
                        console.log("Erro ao listar tarefas abertas! " + response);

                            try{
                            if($scope.tarefa.cliente !== null && $scope.tarefa.cliente !== undefined ){
                                            
                               }else{
                                   $scope.proximaTarefaFiltrada();
                               }

                            }catch{
                                $scope.proximaTarefaFiltrada();

                            }
           
                    });
                }
            }else if ($scope.selectedValue==5 && $scope.slides.length<=0){
                if( $scope.qtdTarefasNaoPossivelObterImagem.length!=0){
                tarefasAPI.filtraNaoPossivelObter().then(function (response) {

                        if (response !== undefined && response.length > 0) {

                            response.forEach(tarefa => {
                                if (tarefa !== undefined) {
                                    tarefa.ultimoProcessamento = new Date(tarefa.ultimoProcessamento).toLocaleDateString();
                                }
                            });

                            $scope.tarefasFiltro = response;
                            $scope.tarefasFiltro.push($scope.tarefa);
                            habilitarFiltro();
                        }
                         $scope.proximaTarefaFiltrada();

                    }, function errorCallback(response) {
                        habilitarFiltro();
                       
                        console.log("Erro ao listar tarefas abertas! " + response);

                          try{
                            if($scope.tarefa.cliente !== null && $scope.tarefa.cliente !== undefined ){
                                            
                               }else{
                                  $scope.proximaTarefaFiltrada();
                               }

                            }catch{
                                $scope.proximaTarefaFiltrada();

                            }
           
                    });

                }
            }
  
    };

  $scope.getSelectValue = function () {
            console.log("testando");
            // var selectedText = $(this).find("option:selected").text();
            $scope.selectedValue = $("#comboFiltro").val();

            if($scope.selectedValue==1){
             
            	$scope.proximaTarefa();
       
            }else if($scope.selectedValue==2){

              if( $scope.qtdTarefasLabelsNao.length!=0){
 
                tarefasAPI.filtraLabelsNaoSatisfazem().then(function (response) {

                        if (response !== undefined && response.length > 0) {

                            response.forEach(tarefa => {
                                if (tarefa !== undefined) {
                                    tarefa.ultimoProcessamento = new Date(tarefa.ultimoProcessamento).toLocaleDateString();
                                }
                            });

                            $scope.tarefasFiltro = response;
                            $scope.tarefasFiltro.push($scope.tarefa);
                            habilitarFiltro();
                        }
                         $scope.proximaTarefaFiltrada();

                    }, function errorCallback(response) {
                        habilitarFiltro();
                       
                        console.log("Erro ao listar tarefas abertas! " + response);

                         try{
                            if($scope.tarefa.cliente !== null && $scope.tarefa.cliente !== undefined ){
                                            
                            }else{
                               $scope.proximaTarefaFiltrada();
                            }

                          }catch{
                            $scope.proximaTarefaFiltrada();

                          }

                    });
                 }

            }
            else if($scope.selectedValue==3 ){

                if( $scope.qtdTarefasErroServidor.length!=0){
                 tarefasAPI.filtraErroConexaoServidor().then(function (response) {

                        if (response !== undefined && response.length > 0) {

                            response.forEach(tarefa => {
                                if (tarefa !== undefined) {
                                    tarefa.ultimoProcessamento = new Date(tarefa.ultimoProcessamento).toLocaleDateString();
                                }
                            });

                            $scope.tarefasFiltro = response;
                            $scope.tarefasFiltro.push($scope.tarefa);
                            habilitarFiltro();
                        }
                         $scope.proximaTarefaFiltrada();

                    }, function errorCallback(response) {
                        habilitarFiltro();
                       
                        console.log("Erro ao listar tarefas abertas! " + response);

                        try{
                            if($scope.tarefa.cliente !== null && $scope.tarefa.cliente !== undefined ){
                                            
                             }else{
                                $scope.proximaTarefaFiltrada();
                             }

                            }catch{
                                $scope.proximaTarefaFiltrada();

                            }
   
                    });

             }
                
            }else if ($scope.selectedValue==4 ){

                if( $scope.qtdTarefasNoReferencias.length!=0){
                tarefasAPI.filtraCameraSem().then(function (response) {

                        if (response !== undefined && response.length > 0) {

                            response.forEach(tarefa => {
                                if (tarefa !== undefined) {
                                    tarefa.ultimoProcessamento = new Date(tarefa.ultimoProcessamento).toLocaleDateString();
                                }
                            });

                            $scope.tarefasFiltro = response;
                            $scope.tarefasFiltro.push($scope.tarefa);
                            habilitarFiltro();
                        }
                         $scope.proximaTarefaFiltrada();

                    }, function errorCallback(response) {
                        habilitarFiltro();
                       
                        console.log("Erro ao listar tarefas abertas! " + response);

                         try{
                            if($scope.tarefa.cliente !== null && $scope.tarefa.cliente !== undefined ){
                                            
                            }else{
                               $scope.proximaTarefaFiltrada();
                            }

                            }catch{
                                $scope.proximaTarefaFiltrada();

                            }
     
                    });
                  }
            }else if ($scope.selectedValue==5 ){
                if( $scope.qtdTarefasNaoPossivelObterImagem.length!=0){
                    tarefasAPI.filtraNaoPossivelObter().then(function (response) {

                        if (response !== undefined && response.length > 0) {

                            response.forEach(tarefa => {
                                if (tarefa !== undefined) {
                                    tarefa.ultimoProcessamento = new Date(tarefa.ultimoProcessamento).toLocaleDateString();
                                }
                            });

                            $scope.tarefasFiltro = response;
                            $scope.tarefasFiltro.push($scope.tarefa);
                            habilitarFiltro();
                        }
                         $scope.proximaTarefaFiltrada();

                    }, function errorCallback(response) {
                        habilitarFiltro();
                       
                        console.log("Erro ao listar tarefas abertas! " + response);

                        try{
                            if($scope.tarefa.cliente !== null && $scope.tarefa.cliente !== undefined ){
                                            
                               }else{
                                  $scope.proximaTarefaFiltrada();
                               }

                            }catch{
                                $scope.proximaTarefaFiltrada();

                            }
                                               
                    });

                 }
            }

    };

     var getHistoricoOS = function () {
        ordemServicoAPI.historicoOS($scope.tarefa)
            .then(function (response) {
                $scope.historicoOs = response;

                /*
				 * if( $scope.qtdTarefas==0){ $("#btnanterior").prop('disabled',
				 * true); $("#btnproximo").prop('disabled', true); }else{
				 * 
				 * $("#btnanterior").prop('disabled', false);
				 * $("#btnproximo").prop('disabled', false); }
				 */
            }, function errorCallback(response) {
                setMsgErro("Erro ao retornar o historico das os desta tarefa!");
                console.log("Erro ao retornar o historico das os desta tarefa!  " + response);
            });

    };
     var getHistoricoTarefas = function () {
        tarefasAPI.historicoTarefas($scope.tarefa.cliente.id)
            .then(function (response) {
                $scope.historicoTarefas = response;

                /*
				 * if( $scope.qtdTarefas==0){ $("#btnanterior").prop('disabled',
				 * true); $("#btnproximo").prop('disabled', true); }else{
				 * 
				 * $("#btnanterior").prop('disabled', false);
				 * $("#btnproximo").prop('disabled', false); }
				 */
            }, function errorCallback(response) {
                setMsgErro("Erro ao retornar o historico das tarefas desta tarefa!");
                console.log("Erro ao retornar o historico das tarefas desta tarefa!  " + response);
            });

    };

    var getQuantidadeTarefasAbertas = function () {
        tarefasAPI.getQuantidadeTarefasAbertas()
            .then(function (response) {
                $scope.qtdTarefas = response;

                /*
				 * if( $scope.qtdTarefas==0){ $("#btnanterior").prop('disabled',
				 * true); $("#btnproximo").prop('disabled', true); }else{
				 * 
				 * $("#btnanterior").prop('disabled', false);
				 * $("#btnproximo").prop('disabled', false); }
				 */
            }, function errorCallback(response) {
                setMsgErro("Erro ao retornar a quantidade de tarefas abertas!");
                console.log("Erro ao retornar a quantidade de tarefas abertas!  " + response);
            });

         getQuantidadeTarefasErroServidor();
         getQuantidadeTarefasNoReferencias();
         getQuantidadeTarefasNaoPossivelObterImagem();
         getQuantidadeTarefasLabelsNao();
         getQuantidadeTarefasImagemReprocessada();
    };

    var getQuantidadeTarefasErroServidor = function () {
        tarefasAPI.getQuantidadeTarefasErroServidor()
            .then(function (response) {
                $scope.qtdTarefasErroServidor = response;
                    /*
					 * if( $scope.qtdTarefasErroServidor==0){
					 * $("#btnanterior").prop('disabled', true);
					 * $("#btnproximo").prop('disabled', true); }else{
					 * 
					 * $("#btnanterior").prop('disabled', false);
					 * $("#btnproximo").prop('disabled', false); }
					 */
            }, function errorCallback(response) {
                setMsgErro("Erro ao retornar a quantidade de tarefas abertas!");
                console.log("Erro ao retornar a quantidade de tarefas abertas!  " + response);
            });

     
    };

   

     var getQuantidadeTarefasNoReferencias = function () {
        tarefasAPI.getQuantidadeTarefasNoReferencias()
            .then(function (response) {
                $scope.qtdTarefasNoReferencias = response;
/*
 * if( $scope.qtdTarefasNoReferencias==0){ $("#btnanterior").prop('disabled',
 * true); $("#btnproximo").prop('disabled', true); }else{
 * 
 * $("#btnanterior").prop('disabled', false); $("#btnproximo").prop('disabled',
 * false); }
 */
            }, function errorCallback(response) {
                setMsgErro("Erro ao retornar a quantidade de tarefas abertas!");
                console.log("Erro ao retornar a quantidade de tarefas abertas!  " + response);
            });

     
    };

    var getQuantidadeTarefasNaoPossivelObterImagem = function () {
        tarefasAPI.getQuantidadeTarefasNaoPossivelObterImagem()
            .then(function (response) {
                $scope.qtdTarefasNaoPossivelObterImagem = response;
/*
 * if( $scope.qtdTarefasNaoPossivelObterImagem==0){
 * $("#btnanterior").prop('disabled', true); $("#btnproximo").prop('disabled',
 * true); }else{
 * 
 * $("#btnanterior").prop('disabled', false); $("#btnproximo").prop('disabled',
 * false); }
 */
            }, function errorCallback(response) {
                setMsgErro("Erro ao retornar a quantidade de tarefas abertas!");
                console.log("Erro ao retornar a quantidade de tarefas abertas!  " + response);
            });

     
    };

    var getQuantidadeTarefasLabelsNao = function () {
        tarefasAPI.getQuantidadeTarefasLabelsNao()
            .then(function (response) {
                $scope.qtdTarefasLabelsNao = response;
/*
 * if( $scope.qtdTarefasLabelsNao==0){ $("#btnanterior").prop('disabled', true);
 * $("#btnproximo").prop('disabled', true); }else{
 * 
 * $("#btnanterior").prop('disabled', false); $("#btnproximo").prop('disabled',
 * false); }
 */
            }, function errorCallback(response) {
                setMsgErro("Erro ao retornar a quantidade de tarefas abertas!");
                console.log("Erro ao retornar a quantidade de tarefas abertas!  " + response);
            });

     
    };

    var getQuantidadeTarefasImagemReprocessada = function () {
        tarefasAPI.getQuantidadeTarefasImagemReprocessada()
            .then(function (response) {
                $scope.qtdTarefasImagemReprocessada = response;
/*
 * if( $scope.qtdTarefasImagemReprocessada==0){
 * $("#btnanterior").prop('disabled', true); $("#btnproximo").prop('disabled',
 * true); }else{
 * 
 * $("#btnanterior").prop('disabled', false); $("#btnproximo").prop('disabled',
 * false); }
 */
            }, function errorCallback(response) {
                setMsgErro("Erro ao retornar a quantidade de tarefas abertas!");
                console.log("Erro ao retornar a quantidade de tarefas abertas!  " + response);
            });

     
    };

    var getQuantidadeOperadoresAtivos = function () {
        tarefasAPI.getQuantidadeOperadoresAtivos()
            .then(function (response) {
                $scope.qtdOperadores = response;
            }, function errorCallback(response) {
               
                console.log("Erro ao retornar a quantidade de tarefas abertas!  " + response);
            });
    };

    var condicaoFiltroCamerasComDefeito = function (imagem) {
        return imagem.descricaoProblema !== null && imagem.descricaoProblema !== undefined;
    };

    var condicaoFiltroCamerasComDefeitoServidor = function (imagem) {
        var contemErro = imagem.descricaoProblema.indexOf("Falha ao obter sessão do servidor") > -1 ? true : false
        if (contemErro === true) {
            result = null;

        } else {
            result = imagem;
        }
        return result;
    };

    var buscarReferencias = function (camera) {

        $scope.msgCamerasReferencia = "Buscando referências...";

        if (camera !== null && camera !== undefined && Object.keys(camera).length > 0) {
            imagensAPI.buscarReferencias(camera)
                .then(function (response) {

                    if (response !== null && response.length > 0) {
                        $scope.slidesReferencias = response;
                        if ($scope.slidesReferencias !== undefined && $scope.slidesReferencias.length > 0) {
                           $scope.totalReferenciasCurrent = $scope.slidesReferencias.length;
                            $scope.slidesReferencias[0].active = true;
                            $scope.labelsImagemReferencia = $scope.slidesReferencias[0].listLabels;
                            pintarLabels();
                        }
                    } else {
                         $scope.totalReferenciasCurrent = 0;
                        limparReferencias();
                    }

                    complete();
                }, function errorCallback(response) {
                    console.log("Erro ao buscar imagens referência: " + response);
                    complete();
                });
        }

    };

    var pintarLabels = function () {
        if ($scope.labelsImagemCamera != undefined && $scope.labelsImagemCamera != null) {
            for (var i = 0; i < $scope.labelsImagemCamera.length; i++) {
                if ($scope.labelsImagemReferencia !== null && $scope.labelsImagemReferencia !== undefined) {
                    var index = $scope.labelsImagemReferencia.findIndex(label => label.descricao === $scope.labelsImagemCamera[i].descricao);

                    if (index > -1) {
                        $scope.labelsImagemReferencia[index].match = true;
                        $scope.labelsImagemCamera[i].match = true;
                    } else {
                        $scope.labelsImagemCamera[i].match = false;
                    }
                } else {
                    $scope.labelsImagemCamera[i].match = false;
                }
            }

            $scope.porcentagemMatchImagem = 0;
            if ($scope.labelsImagemCamera.length > 0) {
                $scope.porcentagemMatchImagem = calcularPorcentagem($scope.labelsImagemCamera);
            }

            $scope.porcentagemMatchImagemRef = 0;
            if ($scope.labelsImagemReferencia.length > 0) {
                $scope.porcentagemMatchImagemRef = calcularPorcentagem($scope.labelsImagemReferencia);
            }
        }

    };

    var limparReferencias = function () {
        $scope.labelsImagemReferencia = [];
        $scope.porcentagemMatchImagem = 0;
        $scope.porcentagemMatchImagemRef = 0;
        $scope.slidesReferencias = [];
        $scope.msgCamerasReferencia = "Câmera não possui imagens referência.";
    };

    var limparLabelsImagem = function () {
        $scope.labelsImagemCamera = [];
    };

    var limparCameraAoVivo = function () {
        $scope.imgAoVivo = undefined;
        $scope.msgErro = undefined;
      
    };

    var salvarOSImagemCamera = function () {

        $scope.disableBtnSalvar = true;
        $scope.os.descricao = $scope.formulario.justificativa;
        $scope.os.codigoUsuario = null;
        $scope.os.nomeUsuario = null;
        $scope.os.usuarioFusion = null;
        $scope.os.cdTecnicoResponsavel = $scope.tarefa.cliente.cdTecnicoResponsavel;
        $scope.os.cdCliente = $scope.tarefa.cliente.cdCliente;
        $scope.os.tarefa = $scope.tarefa.id;

        ordemServicoAPI.salvarOS($scope.os)
            .then(function (response) {

                alert("OS " + response + " aberta com sucesso!");   
                
                setMsgSucesso("OS " + response + " aberta com sucesso!");
                $scope.tarefa.numOS = response;
                salvarImagem();
            }, function errorCallback(response) {
                setMsgErro("Erro ao salvar ordem de serviço!");
                console.log("Erro ao salvar ordem de serviço! " + response);
            });
    };

    $scope.sendRat =  function(){
        if($scope.tarefa.cliente.email==="" || $scope.tarefa.cliente.email===null || $scope.tarefa.cliente.email===undefined){
            
            alert("Não identificamos o Email deste cliente");   
            $("#rat").prop('disabled', true);
        }else{
           if (confirm("INFO: Deseja Realmente enviar uma RAT para este cliente?")) {
               enviarRat();

                } else {
                 
               }
        }
        

    }
   

    var salvarOSImagemCameraAssessoria = function () {
        var total = 0;
        for (var i = 0; i <$scope.slides.length; i++) {
            if($scope.slides[i].falha==true && $scope.slides[i].pathImagem=="erro" ){
                total ++;
            }


        }
        if(total>=$scope.slides.length){
              var txt;
              if (confirm("INFO: Verificamos que TODAS as câmeras deste cliente não foram carregadas. Deseja ABRIR OS para todas? Caso contrário, Clique em Cancelar e tenter REPROCESSAR.")) {
                $scope.disableBtnSalvar = true;
                $scope.os.descricao = $scope.formulario.justificativa;
                $scope.os.codigoUsuario = null;
                $scope.os.nomeUsuario = null;
                $scope.os.usuarioFusion = null;
                $scope.os.cdTecnicoResponsavel = "119322";
                $scope.os.cdCliente = $scope.tarefa.cliente.cdCliente;
                $scope.os.tarefa = $scope.tarefa.id;

                ordemServicoAPI.salvarOS($scope.os)
                    .then(function (response) {
                        alert("OS " + response + " para ASSESSORIA aberta com sucesso!");   
                        setMsgSucesso("OS " + response + " para ASSESSORIA aberta com sucesso!");
                        $scope.tarefa.numOS = response;
                        salvarImagem();
                    }, function errorCallback(response) {
                        setMsgErro("Erro ao salvar ordem de serviço!");
                        console.log("Erro ao salvar ordem de serviço! " + response);
                    });

                    ignorarTodasCamerasComDefeito();
                      } else {
                 $scope.disableBtnSalvar = true;
                $scope.os.descricao = $scope.formulario.justificativa;
                $scope.os.codigoUsuario = null;
                $scope.os.nomeUsuario = null;
                $scope.os.usuarioFusion = null;
                $scope.os.cdTecnicoResponsavel = "119322";
                $scope.os.cdCliente = $scope.tarefa.cliente.cdCliente;
                $scope.os.tarefa = $scope.tarefa.id;

                ordemServicoAPI.salvarOS($scope.os)
                    .then(function (response) {
                        alert("OS " + response + " para ASSESSORIA aberta com sucesso!");   
                        setMsgSucesso("OS " + response + " para ASSESSORIA aberta com sucesso!");
                        $scope.tarefa.numOS = response;
                        salvarImagem();
                    }, function errorCallback(response) {
                        setMsgErro("Erro ao salvar ordem de serviço!");
                        console.log("Erro ao salvar ordem de serviço! " + response);
                    });
                      }
        }else if (total>2){
            var inserido = 0;
            if (confirm("INFO: Verificamos que ALGUMAS câmeras deste cliente não foram carregadas. Deseja ABRIR OS para todas? Caso contrário, Clique em Cancelar e tenter REPROCESSAR.")) {
              
              for (var i = 0; i <$scope.slides.length; i++) {
                  if($scope.slides[i].falha==true && $scope.slides[i].pathImagem=="erro" ){
                    $scope.disableBtnSalvar = true;
                    $scope.os.descricao = $scope.formulario.justificativa;
                    $scope.os.codigoUsuario = null;
                    $scope.os.nomeUsuario = null;
                    $scope.os.usuarioFusion = null;
                    $scope.os.cdTecnicoResponsavel = "119322";
                    $scope.os.cdCliente = $scope.tarefa.cliente.cdCliente;
                    $scope.os.tarefa = $scope.tarefa.id;

                    if(inserido==0){
                         ordemServicoAPI.salvarOS($scope.os)
                            .then(function (response) {
                                alert("OS " + response + " para ASSESSORIA aberta com sucesso!");   
                                setMsgSucesso("OS " + response + " para ASSESSORIA aberta com sucesso!");
                                $scope.tarefa.numOS = response;
                                salvarImagem();
                            }, function errorCallback(response) {
                                setMsgErro("Erro ao salvar ordem de serviço!");
                                console.log("Erro ao salvar ordem de serviço! " + response);
                            });
                            inserido++;
                             start();

                        var indexSlide = i;

                        imagensAPI.salvarImagem($scope.slides[i])
                            .then(function (response) {
                                $scope.tarefa.listImagens[indexSlide] = response;
                                encerrarTarefa();
                            }, function errorCallback(response) {
                                setMsgErro("Erro ao salvar dados da tarefa!");
                                console.log("Erro ao salvar dados da tarefa! " + response);
                                complete();
                            });  
                    }else{

                         start();

                        var indexSlide = i;

                        imagensAPI.salvarImagem($scope.slides[i])
                            .then(function (response) {
                                $scope.tarefa.listImagens[indexSlide] = response;
                                encerrarTarefa();
                            }, function errorCallback(response) {
                                setMsgErro("Erro ao salvar dados da tarefa!");
                                console.log("Erro ao salvar dados da tarefa! " + response);
                                complete();
                            });  
                    }
                        
                    }}
                      
                     

                    } else {
                             $scope.disableBtnSalvar = true;
                            $scope.os.descricao = $scope.formulario.justificativa;
                            $scope.os.codigoUsuario = null;
                            $scope.os.nomeUsuario = null;
                            $scope.os.usuarioFusion = null;
                            $scope.os.cdTecnicoResponsavel = "119322";
                            $scope.os.cdCliente = $scope.tarefa.cliente.cdCliente;
                            $scope.os.tarefa = $scope.tarefa.id;

                            ordemServicoAPI.salvarOS($scope.os)
                                .then(function (response) {
                                    alert("OS " + response + " para ASSESSORIA aberta com sucesso!");   
                                    setMsgSucesso("OS " + response + " para ASSESSORIA aberta com sucesso!");
                                    $scope.tarefa.numOS = response;
                                    salvarImagem();
                                }, function errorCallback(response) {
                                    setMsgErro("Erro ao salvar ordem de serviço!");
                                    console.log("Erro ao salvar ordem de serviço! " + response);
                                });
                                  }
                                   onInit();

        }


        else{
            $scope.disableBtnSalvar = true;
                $scope.os.descricao = $scope.formulario.justificativa;
                $scope.os.codigoUsuario = null;
                $scope.os.nomeUsuario = null;
                $scope.os.usuarioFusion = null;
                $scope.os.cdTecnicoResponsavel = "119322";
                $scope.os.cdCliente = $scope.tarefa.cliente.cdCliente;
                $scope.os.tarefa = $scope.tarefa.id;

                ordemServicoAPI.salvarOS($scope.os)
                    .then(function (response) {
                        alert("OS " + response + " para ASSESSORIA aberta com sucesso!");   
                        setMsgSucesso("OS " + response + " para ASSESSORIA aberta com sucesso!");
                        $scope.tarefa.numOS = response;
                        salvarImagem();
                    }, function errorCallback(response) {
                        setMsgErro("Erro ao salvar ordem de serviço!");
                        console.log("Erro ao salvar ordem de serviço! " + response);
                    });
                      

        }
        
       
    };


    var salvarOSClienteComFalhaAssessoria = function () {
        $scope.disableBtnSalvar = true;
        $scope.os.descricao = $scope.formulario.justificativa;
        $scope.os.codigoUsuario = null;
        $scope.os.nomeUsuario = null;
        $scope.os.usuarioFusion = null;
        $scope.os.cdTecnicoResponsavel = "119322";
        $scope.os.cdCliente = $scope.tarefa.cliente.cdCliente;
        $scope.os.tarefa = $scope.tarefa.id;

        ordemServicoAPI.salvarOS($scope.os)
            .then(function (response) {
                alert("OS " + response + " para ASSESSORIA aberta com sucesso!"); 
                setMsgSucesso("OS " + response + " para ASSESSORIA aberta com sucesso!");
                $scope.tarefa.numOS = response;
                encerrarTarefaComFalhaAssessoria();
            }, function errorCallback(response) {
                setMsgErro("Erro ao salvar ordem de serviço!");
                console.log("Erro ao salvar ordem de serviço! " + response);
            });
    };

   var updateTesteAutomatico = function () {

        if (confirm("INFO: Ao confirmar esta ação o cliente não mais será processado pelo CFTV. Esta ação é válida caso o motivo das tarefas com problema sejam por TERMOS abertos. Deseja confirmar?")) {
              $scope.disableBtnSalvar = true;
                        $scope.os.descricao = $scope.formulario.justificativa;
                        $scope.os.codigoUsuario = null;
                        $scope.os.nomeUsuario = null;
                        $scope.os.usuarioFusion = null;
                        $scope.os.cdTecnicoResponsavel = "119322";
                        $scope.os.cdCliente = $scope.tarefa.cliente.cdCliente;
                        $scope.os.tarefa = $scope.tarefa.id;

                        tarefasAPI.updateTesteAutomatico($scope.tarefa.cliente.cdCliente)
                            .then(function (response) {
                                alert("Info " + response + " cliente desabilitado com sucesso!"); 
                                setMsgSucesso("Info " + response + " cliente desabilitado com sucesso!");
                                encerrarTarefaDesabilitarCliente();
                            }, function errorCallback(response) {
                                setMsgErro("Erro ao desabilitar cliente!");
                                console.log("Erro ao desabilitar cliente! " + response);
                            });
                      } else {

                      }
        
    };

    var salvarOSClienteComFalha = function () {
        $scope.disableBtnSalvar = true;
        $scope.os.descricao = $scope.formulario.justificativa;
        $scope.os.codigoUsuario = null;
        $scope.os.nomeUsuario = null;
        $scope.os.usuarioFusion = null;
        $scope.os.cdTecnicoResponsavel = $scope.tarefa.cliente.cdTecnicoResponsavel;
        $scope.os.cdCliente = $scope.tarefa.cliente.cdCliente;
        $scope.os.tarefa = $scope.tarefa.id;

        ordemServicoAPI.salvarOS($scope.os)
            .then(function (response) {
                alert("OS " + response + " para ASSESSORIA aberta com sucesso!");
                setMsgSucesso("OS " + response + " aberta com sucesso!");
                $scope.tarefa.numOS = response;
                encerrarTarefaComFalha();
            }, function errorCallback(response) {
                setMsgErro("Erro ao salvar ordem de serviço!");
                console.log("Erro ao salvar ordem de serviço! " + response);
            });
    };

    var start = function () {
        cfpLoadingBar.start();
    };

    var complete = function () {
        cfpLoadingBar.complete();
        $scope.loading = false;
    };


    var filtrarCheckbox = function () {
        $scope.countDefeito =0;
        getQuantidadeTarefasAbertas();
         $scope.qtdOperadores= getQuantidadeOperadoresAtivos();
        if (!$scope.tarefa.falha) {
            $timeout(function () {

                limparReferencias();
                limparLabelsImagem();
                limparCameraAoVivo();
                try{
                     var indexSlideSelecionado = $scope.slides.indexOf($scope.slideSelecionado);
                if ($scope.tarefa.listImagens !== undefined) {
                    if ($scope.checkedSomenteCamerasDefeito) {
                        $scope.slides = $scope.tarefa.listImagens.filter(condicaoFiltroCamerasComDefeito);
                        $scope.slideSelecionado = $scope.slides[indexSlideSelecionado];

                          atualizarElementos();


                    } else {
                        $scope.slides = $scope.tarefa.listImagens;
                        $scope.slideSelecionado = $scope.slides[indexSlideSelecionado];
                        atualizarElementos();
                    }
                }
                }catch{
                 if ($scope.tarefa.listImagens !== undefined) {
                    if ($scope.checkedSomenteCamerasDefeito) {
                        $scope.slides = $scope.tarefa.listImagens.filter(condicaoFiltroCamerasComDefeito);
                        // $scope.slideSelecionado =
						// $scope.slides[indexSlideSelecionado];

                          atualizarElementos();


                    } else {
                        $scope.slides = $scope.tarefa.listImagens;
                        $scope.slideSelecionado = $scope.slides[indexSlideSelecionado];
                        atualizarElementos();
                    }
                }

                }
               

                
            }, 300);

        } else {
            $scope.selecionarAcaoClienteComFalha(2);
        }

        for (var i = 0; i < $scope.tarefa.listImagens.length; i++){
            if($scope.tarefa.listImagens[i].descricaoProblema !== null && $scope.tarefa.listImagens[i].descricaoProblema !== undefined){
                
                $scope.countDefeito++;
            }
           
        }
        $scope.countTotal = $scope.tarefa.listImagens.length;

        if($scope.countDefeito==0 && $scope.tarefa.listImagens.length !=0){
        limparCameraAoVivo();
        limparLabelsImagem();
        limparReferencias();
      
        ignorarTodasCamerasComDefeito();
        salvarTarefa();
        
        $scope.proximaTarefa();
        }else if($scope.countDefeito==0 && $scope.slideSelecionado.descricaoProblema !== null || $scope.slideSelecionado.descricaoProblema!== undefined){

            if($scope.flagDefeito !=0){
                $scope.flagDefeito =0;
            }else{
                 
                
               
                // $scope.proximaTarefa();
                  // $scope.encerrarTarefa();
            }
            
            
        }else if($scope.countDefeito==1){
             $scope.selecionarAcao(5);
             $("#salvarReferencia").prop('disabled', true);
        }

        else{

                if ($scope.slideSelecionado.listLabels.length < 3) {
                    if ($scope.slideSelecionado.listLabels.length == 0 || $scope.slideSelecionado.listLabels.length == undefined){
                            complete();
                             $scope.formulario.justificativa = "Câmera " + $scope.slideSelecionado.camera.nomeCamera + " com defeito. ATENÇÃO Verificar todas as câmeras desse cliente!";
                             $scope.selecionarAcao(5);
                             $("#salvarReferencia").prop('disabled', true);
                             $scope.selecionarAcao(5);
                    }else{
                         complete();
                         $scope.formulario.justificativa = "Câmera " + $scope.slideSelecionado.camera.nomeCamera + " com defeito.  ATENÇÃO Verificar todas as câmeras desse cliente!";
                         $scope.selecionarAcao(3);
                         $("#salvarReferencia").prop('disabled', true);
                          $scope.selecionarAcao(3);
                    }
                    

                   

                }else{

                    var k = 0;
                    for (var j = 0; j < $scope.slideSelecionado.listLabels.length; j++) {

                        if ($scope.slideSelecionado.listLabels[j].descricao === "Text" &&  $scope.slideSelecionado.listLabels.length <3) {
                             $scope.selecionarAcao(3);
                              $("#salvarReferencia").prop('disabled', true);
                          
                            complete();
                            break;
                           

                        } else if ($scope.slideSelecionado.listLabels[j].descricao === "Call of Duty" ||
                            
                            
                            $scope.slideSelecionado.listLabels[j].descricao === "The Legend of Zelda" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Grand Theft Auto" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Final Fantasy" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Doom" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Minecraft" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Overwatch" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Gray" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "World Of Warcraft" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Text" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Symbol" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Unreal Tournament" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Smoke" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Fog" 
                            

                        ) {
                            k++;

                        }

                    }

                    var total = j * 0.15;

                    if (k >= total) {
                    $scope.selecionarAcao(3);
                    $("#salvarReferencia").prop('disabled', true);
                   
                    }else{
                         $scope.selecionarAcao(1);
                         $("#salvarReferencia").prop('disabled', false);

                    }
                }

                    
        }

	    getHistoricoOS();
	    getHistoricoTarefas();
	    getRatsEnviadas();
    };

    var filtrarCheckboxServidor = function () {

        if ($scope.tarefa.falha) {
            $timeout(function () {
                limparReferencias();
                limparLabelsImagem();
                limparCameraAoVivo();

                var indexSlideSelecionado = $scope.slides.indexOf($scope.slideSelecionado);

                if ($scope.tarefa.listImagens !== undefined) {
                    if ($scope.checkboxErroServidor) {
                        $scope.slides = $scope.tarefa.listImagens.filter(condicaoFiltroCamerasComDefeitoServidor);
                        $scope.slideSelecionado = $scope.slides[indexSlideSelecionado];

                        atualizarElementos();
                    } else {
                        $scope.slides = $scope.tarefa.listImagens;
                        $scope.slideSelecionado = $scope.slides[indexSlideSelecionado];
                        atualizarElementos();
                    }
                }
            }, 300);

        } else {
            $scope.selecionarAcaoClienteComFalha(2);
        }
    };

    var encerrarTarefaComFalha = function () {
        $scope.tarefa.descricaoFalha = $scope.formulario.justificativa;
        $scope.tarefa.dataEncerramento = new Date().getTime();
        $scope.tarefa.status = 0;

       
        tarefasAPI.salvarTarefa($scope.tarefa)
            .then(function (response) {
                if (response.status === 0) {
                    getTarefa();
                } else {
                    filtrarCheckbox();
                }
                getQuantidadeTarefasAbertas();
                 $scope.qtdOperadores= getQuantidadeOperadoresAtivos();

            }, function errorCallback(response) {
                setMsgErro("Erro ao salvar dados da tarefa!");
                console.log("Erro ao salvar dados da tarefa! " + response);
                complete();
            });
    };

    var encerrarTarefaComFalhaAssessoria = function () {
        $scope.tarefa.descricaoFalha = $scope.formulario.justificativa;
        $scope.tarefa.dataEncerramento = new Date().getTime();
        $scope.tarefa.status = 0;
       
        tarefasAPI.salvarTarefa($scope.tarefa)
            .then(function (response) {
                if (response.status === 0) {
                    getTarefa();
                } else {
                    filtrarCheckbox();
                }
                getQuantidadeTarefasAbertas();
                 $scope.qtdOperadores= getQuantidadeOperadoresAtivos();

            }, function errorCallback(response) {
                setMsgErro("Erro ao salvar dados da tarefa!");
                console.log("Erro ao salvar dados da tarefa! " + response);
                complete();
            });
    };

    var encerrarTarefaDesabilitarCliente = function () {
        $scope.tarefa.descricaoFalha = $scope.formulario.justificativa;
        $scope.tarefa.dataEncerramento = new Date().getTime();
        $scope.tarefa.status = 0;
       
        tarefasAPI.salvarTarefa($scope.tarefa)
            .then(function (response) {
                if (response.status === 0) {
                    getTarefa();
                } else {
                    filtrarCheckbox();
                }
                getQuantidadeTarefasAbertas();
                 $scope.qtdOperadores= getQuantidadeOperadoresAtivos();

            }, function errorCallback(response) {
                setMsgErro("Erro ao salvar dados da tarefa!");
                console.log("Erro ao salvar dados da tarefa! " + response);
                complete();
            });
    };

    var encerrarTarefa = function () {
        start();

        var qtdProblemas = $scope.tarefa.listImagens.filter(condicaoFiltroCamerasComDefeito);

        if (qtdProblemas.length == 0) {
            $scope.tarefa.dataEncerramento = new Date().getTime();
            $scope.tarefa.status = 0;
            $scope.slides = [];
            $scope.slideSelecionado = {};

            var indexTarefaFiltro = $scope.tarefasMemoria.indexOf($scope.tarefa.id);

            if (indexTarefaFiltro > -1) {
                $scope.tarefasMemoria.splice((indexTarefaFiltro - 1), 1);
            }
         
            tarefasAPI.salvarTarefa($scope.tarefa)
                .then(function (response) {
                    if (response.status === 0) {
                        getTarefa();
                        // } else {
                        // filtrarCheckbox();
                    }
                    getQuantidadeTarefasAbertas();
                     $scope.qtdOperadores= getQuantidadeOperadoresAtivos();
                    $scope.disableBtnSalvar = false;

                }, function errorCallback(response) {
                    setMsgErro("Erro ao salvar dados da tarefa!");
                    console.log("Erro ao salvar dados da tarefa! " + response);
                    complete();
                });
        } else {
            filtrarCheckbox();
        }
    };



    var salvarImagem = function () {
        start();

        var indexSlide = $scope.tarefa.listImagens.indexOf($scope.slideSelecionado);

        imagensAPI.salvarImagem($scope.slideSelecionado)
            .then(function (response) {
                $scope.tarefa.listImagens[indexSlide] = response;
                encerrarTarefa();
            }, function errorCallback(response) {
                setMsgErro("Erro ao salvar dados da tarefa!");
                console.log("Erro ao salvar dados da tarefa! " + response);
                complete();
            });
    }

     var enviarRat = function () {
        start();
        $scope.tarefa.descricaoFalha = $scope.formulario.justificativa;

        tarefasAPI.enviarRat($scope.tarefa)
            .then(function (response) {
                alert("RAT enviada com sucesso  para " + $scope.tarefa.cliente.email);
                $("#rat").prop('disabled', true);
            }, function errorCallback(response) {
                setMsgErro("Erro ao salvar rat da tarefa!");
                console.log("Erro ao salvar rat da tarefa! " + response);
                complete();
                $("#rat").prop('disabled', false);
            });
    }

    var getRatsEnviadas = function () {
        start();
      
        tarefasAPI.getRatsEnviadas($scope.tarefa.cliente.cpfCnpj)
            .then(function (response) {
              $scope.historicoRats = response.result;
               for (var j = 0; j < $scope.historicoRats.length; j++) {
       
                $scope.historicoRats[j].body = $scope.historicoRats[j].body.replace(/\n/gi,'');
                $scope.historicoRats[j].body = $scope.historicoRats[j].body.replace(/\n/gi,'');
                $scope.historicoRats[j].body = $scope.historicoRats[j].body.replace(/\t/gi,'');
                $scope.historicoRats[j].body = $scope.historicoRats[j].body.replace(/\//gi,'');
                $scope.historicoRats[j].body = $scope.historicoRats[j].body.replace(/table width="100%"/gi,'table width="100%" style="width: 100%; float: left;"');
                $scope.historicoRats[j].body = $scope.historicoRats[j].body.replace(/<table class="bodytbl"/gi,'<table class="bodytbl" style="width:100%">');
                $scope.historicoRats[j].body = $scope.historicoRats[j].body.replace(/(&nbsp;|<([^>]+)>)/ig, "");
                $scope.historicoRats[j].body = $scope.historicoRats[j].body.replace(/&#9733;/ig, "\n");
                $scope.historicoRats[j].body =  $scope.historicoRats[j].body.replace(/width='100%'' cellpadding='0' cellspacing='0'>/ig, "");
                $scope.historicoRats[j].body =  $scope.historicoRats[j].body.replace(/Portal do Cliente | Relatórios de Eventos Atualização Cadastral/ig, "");
                    
                    
               }
              
            }, function errorCallback(response) {
                setMsgErro("Erro ao salvar rat da tarefa!");
                console.log("Erro ao salvar rat da tarefa! " + response);
                complete();
                $("#rat").prop('disabled', false);
            });
    }



    var salvarTarefa = function () {
        start();

        $scope.disableBtnSalvar = true;

        var qtdProblemas = $scope.tarefa.listImagens.filter(condicaoFiltroCamerasComDefeito);

        if (qtdProblemas.length == 0) {
            $scope.tarefa.dataEncerramento = new Date().getTime();
            $scope.tarefa.status = 0;
            $scope.slides = [];
            $scope.slideSelecionado = {};

            var indexTarefaFiltro = $scope.tarefasMemoria.indexOf($scope.tarefa.id);
            if (indexTarefaFiltro > -1) {
                $scope.tarefasMemoria.splice((indexTarefaFiltro - 1), 1);
            }


        }

       

        tarefasAPI.salvarTarefa($scope.tarefa)
            .then(function (response) {
                if (response.status === 0) {
                    getTarefa();
                } else {
                    filtrarCheckbox();
                }
                getQuantidadeTarefasAbertas();
                 $scope.qtdOperadores= getQuantidadeOperadoresAtivos();
                $scope.disableBtnSalvar = false;

            }, function errorCallback(response) {
                setMsgErro("Erro ao salvar dados da tarefa!");
                console.log("Erro ao salvar dados da tarefa! " + response);
                complete();
            });
    };

    var fecharInfoCliente = function () {
        $('#collapseInfoCliente').collapse('hide');
    };

    var draggableModal = function () {
        $("#myModalAoVivo").draggable({
            handle: ".modal-header"
        });
    };

    var draggableModal = function () {
        $("#myModalAguarde").draggable({
            handle: ".modal-header"
        });
    };

    var selecionarBtnSalvarReferencia = function () {
        $("#salvarReferencia").addClass("active glyphicon glyphicon-ok");
        $("#abrirOS").removeClass("active glyphicon glyphicon-ok");
        $("#ignorar").removeClass("active glyphicon glyphicon-ok");
        $("#ignorarTodas").removeClass("active glyphicon glyphicon-ok");
        $("#abrirOSAssessoria").removeClass("active glyphicon glyphicon-ok");
        $("#desativarCliente").removeClass("active glyphicon glyphicon-ok");
        
    };

    var selecionarBtnAbrirOS = function () {
        $("#abrirOS").addClass("active glyphicon glyphicon-ok");
        $("#salvarReferencia").removeClass("active glyphicon glyphicon-ok");
        $("#ignorar").removeClass("active glyphicon glyphicon-ok");
        $("#ignorarTodas").removeClass("active glyphicon glyphicon-ok");
        $("#abrirOSAssessoria").removeClass("active glyphicon glyphicon-ok");
        $("#desativarCliente").removeClass("active glyphicon glyphicon-ok");
    };

    var selecionarBtnAbrirOSAssessoria = function () {
        $("#abrirOSAssessoria").addClass("active glyphicon glyphicon-ok");
        $("#salvarReferencia").removeClass("active glyphicon glyphicon-ok");
        $("#ignorar").removeClass("active glyphicon glyphicon-ok");
        $("#ignorarTodas").removeClass("active glyphicon glyphicon-ok");
        $("#abrirOS").removeClass("active glyphicon glyphicon-ok");
        $("#desativarCliente").removeClass("active glyphicon glyphicon-ok");
    };



    var selectionarBtnIgnorar = function () {
        $("#ignorar").addClass("active glyphicon glyphicon-ok");
        $("#abrirOS").removeClass("active glyphicon glyphicon-ok");
        $("#salvarReferencia").removeClass("active glyphicon glyphicon-ok");
        $("#ignorarTodas").removeClass("active glyphicon glyphicon-ok");
        $("#abrirOSAssessoria").removeClass("active glyphicon glyphicon-ok");
        $("#desativarCliente").removeClass("active glyphicon glyphicon-ok");
    };

    var selecionarBtnIgnorarTodas = function () {
        $("#ignorarTodas").addClass("active glyphicon glyphicon-ok");
        $("#abrirOS").removeClass("active glyphicon glyphicon-ok");
        $("#ignorar").removeClass("active glyphicon glyphicon-ok");
        $("#salvarReferencia").removeClass("active glyphicon glyphicon-ok");
        $("#abrirOSAssessoria").removeClass("active glyphicon glyphicon-ok");
        $("#desativarCliente").removeClass("active glyphicon glyphicon-ok");
    };
    var selectionarBtnDesativar = function () {
        $("#ignorar").removeClass("active glyphicon glyphicon-ok");
        $("#abrirOS").removeClass("active glyphicon glyphicon-ok");
        $("#salvarReferencia").removeClass("active glyphicon glyphicon-ok");
        $("#ignorarTodas").removeClass("active glyphicon glyphicon-ok");
        $("#abrirOSAssessoria").removeClass("active glyphicon glyphicon-ok");
        $("#desativarCliente").addClass("active glyphicon glyphicon-ok");
    };


    var desabilitarFiltro = function () {
        $(document).ready(function () {
            $("#inputFiltro *").prop('disabled', true);
        });
    };

    var habilitarFiltro = function () {
        $("#inputFiltro *").prop('disabled', false);
    };

    var atualizarElementos = function () {
        $scope.showBotoes = false;
        $scope.showForm = false;
        var ac3 = 0;
        var ac4 = 0;
        if ($scope.slideSelecionado === undefined) {
            $scope.slideSelecionado = $scope.slides[0];
        }
        
        else if ($scope.slideSelecionado.listLabels !== undefined && $scope.slideSelecionado.listLabels !== null && $scope.slideSelecionado.listLabels.length === 0) {
          
          try{
                 if($scope.slideSelecionado.pathImagem=="erro"){
                    $scope.selecionarAcao(5);
                    $scope.showBotoes = true;
                    $scope.showForm = true;
                 }else{
                     $scope.selecionarAcao(3);
                    ac3++;
                    $scope.showBotoes = true;
                    $scope.showForm = true;
                 }
          }catch{
            $scope.selecionarAcao(3);
            ac3++;
            $scope.showBotoes = true;
            $scope.showForm = true;

          }
           
           
        }

        else if ($scope.slideSelecionado.descricaoProblema !== undefined && $scope.slideSelecionado.descricaoProblema !== null) {
            $scope.showBotoes = true;
            $scope.showForm = true;
            var text = 0;
            var k = 0;

             if ($scope.slideSelecionado.listLabels.length < 3) {
                  if ($scope.slideSelecionado.listLabels.length == 0 || $scope.slideSelecionado.listLabels.length == undefined){
                            complete();
                             $scope.formulario.justificativa = "Câmera " + $scope.slideSelecionado.camera.nomeCamera + " com defeito.  ATENÇÃO Verificar todas as câmeras desse cliente!";
                             $scope.selecionarAcao(5);
                             $("#salvarReferencia").prop('disabled', true);
                             $scope.selecionarAcao(5);
                    }else{
                         complete();
                         $scope.formulario.justificativa = "Câmera " + $scope.slideSelecionado.camera.nomeCamera + " com defeito.  ATENÇÃO Verificar todas as câmeras desse cliente!";
                         $scope.selecionarAcao(3);
                         $("#salvarReferencia").prop('disabled', true);
                          $scope.selecionarAcao(3);
                    }

                }else{
                    for (var j = 0; j < $scope.slideSelecionado.listLabels.length; j++) {

                        if ($scope.slideSelecionado.listLabels[j].descricao === "Text"  &&  $scope.slideSelecionado.listLabels.length <3) {
                             $scope.selecionarAcao(5);
                              $("#salvarReferencia").prop('disabled', true);
                             text++;
                           
                          

                        } else if ($scope.slideSelecionado.listLabels[j].descricao === "Call of Duty" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "The Legend of Zelda" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Grand Theft Auto" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Final Fantasy" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Doom" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Minecraft" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Overwatch" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Gray" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "World Of Warcraft" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Text" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Symbol" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Unreal Tournament" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Smoke" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Fog" 

                        ) {
                            k++;

                        }

                    }

                    var total = j * 0.15;

                    if (k >= total) {
                    $scope.selecionarAcao(3);
                    $("#salvarReferencia").prop('disabled', true);
                     ac4++;

                    }else{
                            if(text>0 && $scope.slideSelecionado.listLabels.length <=3){
                              $scope.selecionarAcao(3);
                              $("#salvarReferencia").prop('disabled', true);

                            }else{

                         $scope.selecionarAcao(1);
                         $("#salvarReferencia").prop('disabled', false);
                   }

                    }
                }
        }

        else if($scope.checkedSomenteCamerasDefeito==true){
             $scope.showBotoes = true;
             $scope.showForm = true;

             var text = 0;
             var k = 0;
             var total = 0;
              if ($scope.slideSelecionado.listLabels.length < 3) {
                   if ($scope.slideSelecionado.listLabels.length == 0 || $scope.slideSelecionado.listLabels.length == undefined){
                        complete();
                             $scope.formulario.justificativa = "Câmera " + $scope.slideSelecionado.camera.nomeCamera + " com defeito.  ATENÇÃO Verificar todas as câmeras desse cliente!";
                             $scope.selecionarAcao(5);
                              $("#salvarReferencia").prop('disabled', true);
                             $scope.selecionarAcao(5);
                    }else{
                         complete();
                         $scope.formulario.justificativa = "Câmera " + $scope.slideSelecionado.camera.nomeCamera + " com defeito.  ATENÇÃO Verificar todas as câmeras desse cliente!";
                         $scope.selecionarAcao(3);
                         $("#salvarReferencia").prop('disabled', true);
                          $scope.selecionarAcao(3);
                    }

                }else{
                    for (var j = 0; j < $scope.slideSelecionado.listLabels.length; j++) {

                        if ($scope.slideSelecionado.listLabels[j].descricao === "Text" &&  $scope.slideSelecionado.listLabels.length <3) {
                             $scope.selecionarAcao(5);
                              $("#salvarReferencia").prop('disabled', true);
                             text++;
                           
                            break;

                        } else if ($scope.slideSelecionado.listLabels[j].descricao === "Call of Duty" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "The Legend of Zelda" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Grand Theft Auto" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Final Fantasy" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Doom" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Minecraft" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Overwatch" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Gray" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "World Of Warcraft" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Text" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Symbol" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Unreal Tournament" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Smoke" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Fog" 

                        ) {
                            k++;

                        }

                    }

                     total = j * 0.15;

                    if (k >= total) {
                        $scope.selecionarAcao(3);
                        $("#salvarReferencia").prop('disabled', true);
                        ac4++;

                    }else{
                            if(text>0 && $scope.slideSelecionado.listLabels.length <=3){
                              $scope.selecionarAcao(3);
                              $("#salvarReferencia").prop('disabled', true);

                            }else{

                         $scope.selecionarAcao(1);
                         $("#salvarReferencia").prop('disabled', false);
                   }

                    }
                }

                }

            try{
                if($scope.slideSelecionado.listLabels!== undefined){
                            $scope.flagDefeito ++;
                            $scope.labelsImagemCamera = $scope.slideSelecionado.listLabels;
                            $scope.pathImagemCamera = $scope.slideSelecionado.pathImagem;
                            buscarReferencias($scope.slideSelecionado.camera);
                            
                    }}catch {

                        if($scope.formulario.justificativa !== undefined || $scope.formulario.justificativa !== null ){
                            
                                 $scope.selecionarAcaoClienteComFalha(5);
                                $("#salvarReferencia").prop('disabled', true);
                                $scope.flagDefeito ++;
                                                                
                            

                        }

                    }
            };
        
 

    var atualizarElementosReprocessar = function () {


        if ($scope.slideSelecionado === undefined) {
            $scope.slideSelecionado = $scope.slides[0];
        }

        $scope.selecionarAcao(1);

        if ($scope.slideSelecionado.descricaoProblema !== undefined && $scope.slideSelecionado.descricaoProblema !== null) {
            $scope.showBotoes = true;
            $scope.showForm = true;
        }

        $scope.labelsImagemCamera = $scope.slideSelecionado.listLabels;
        $scope.pathImagemCamera = $scope.slideSelecionado.pathImagem;
        buscarReferencias($scope.slideSelecionado.camera);
    };

    var calcularPorcentagem = function (labels) {
        var qtdeLabels = labels.length;
        var qtdeMatch = 0;

        for (var i = 0; i < labels.length; i++) {
            if (labels[i].match === true) {
                qtdeMatch = qtdeMatch + 1;
            }
        }
        return parseFloat(((qtdeMatch * 100) / qtdeLabels).toFixed(2));
    };

    var ignorarTodasCamerasComDefeito = function () {
        var slidesIgnorados = $scope.tarefa.listImagens.filter(condicaoFiltroCamerasComDefeito);
        slidesIgnorados.forEach(function (slides) {
            slides.referencia = false;
            slides.justificativa = $scope.formulario.justificativa;
            slides.descricaoProblema = null;
            slides.tipoTratamento = "Ignorar Todas";

            imagensAPI.salvarImagem(slides)
                .then(function (response) {
                }, function errorCallback(response) {
                    setMsgErro("Erro ao salvar dados da tarefa!");
                    console.log("Erro ao salvar dados da tarefa! " + response);
                    complete();
                });
        });

        encerrarTarefa();
    };

    $scope.tarefaSelecionada = function (selected) {
        if (selected) {
            start();
            tarefasAPI.getTarefaFiltro(selected.originalObject.id, $scope.tarefa.id, $rootScope.operador)
                .then(function (response) {
                    if (response == null) {
                        setMsgErro("Tarefa não encontra-se disponível");
                        getTarefa();
                    } else {
                        $scope.tarefasMemoria.push(response.id);
                        $scope.tarefa = response;
                        $scope.slides = $scope.tarefa.listImagens;
                        $scope.msgCameras = "";
                        $scope.msgCamerasReferencia = "";
                        filtrarCheckbox();
                    }
                    complete();
                }).catch(function (error) {
                    console.log(error);
                    complete();
                });
        }
    };

    $scope.tarefaSelecionadaHistory = function (selected) {
        if (selected) {
            start();
            tarefasAPI.getTarefaFiltroHistory(selected.originalObject.id, $scope.tarefa.id, $rootScope.operador)
                .then(function (response) {
                    if (response == null) {
                        setMsgErro("Tarefa não encontra-se disponível");
                        getTarefa();
                    } else {
                        $scope.tarefasMemoria.push(response.id);
                        $scope.tarefa = response;
                        $scope.slides = $scope.tarefa.listImagens;
                        $scope.msgCameras = "";
                        $scope.msgCamerasReferencia = "";
                        filtrarCheckbox();
                    }
                    complete();
                }).catch(function (error) {
                    console.log(error);
                    complete();
                });
        }
    };

    $scope.deletarReferencia = function (imagemReferencia) {
        var confirmacao = confirm("Deseja deletar a imagem de referência selecionada?");

        if (confirmacao) {

            imagensAPI.deletarReferencia(imagemReferencia)
                .then(function (response) {

                    if (response) {
                        buscarReferencias($scope.slideSelecionado.camera);
                    } else {
                        setMsgErro("Erro ao deletar imagem de referência!");
                    }
                },
                    function errorCallback(response) {
                        setMsgErro("Erro ao deletar imagem de referência!");
                        console.log("Erro ao deletar imagem de referência! " + response);
                        complete();
                    });
        }
    };

    $scope.deletarTodasReferencias = function (imagemReferencia) {
        var confirmacao = confirm("Deseja deletar TODAS as referências deste cliente?");

        if (confirmacao) {
            for (var i = 0; i < $scope.slidesReferencias.length; i++) {

                imagensAPI.deletarReferencia($scope.slidesReferencias[i])
                    .then(function (response) {

                        if (response) {
                            buscarReferencias($scope.slideSelecionado.camera);
                        } else {
                            setMsgErro("Erro ao deletar referências!");
                        }
                    },
                        function errorCallback(response) {

                            console.log("Erro ao deletar referências! " + response);
                            complete();
                        });
            }

        }
    };

    $scope.selecionarAcao = function (acaoSelecionada) {
        $scope.formulario.acao = acaoSelecionada;

        if (acaoSelecionada === 1) {
            $scope.formulario.justificativa = "Imagem salva como refêrencia.";
            selecionarBtnSalvarReferencia();

        } else if (acaoSelecionada === 2) {
            $scope.formulario.justificativa = "Câmera " + $scope.slideSelecionado.camera.nomeCamera + " com defeito.";
            selecionarBtnAbrirOS();

        } else if (acaoSelecionada === 3) {
            $scope.formulario.justificativa = "Imagem câmera " + $scope.slideSelecionado.camera.idCamera + " ignorada.";
            selectionarBtnIgnorar();

        } else if (acaoSelecionada === 4) {
            $scope.formulario.justificativa = "Ignorar todas câmeras do cliente " + $scope.tarefa.cliente.fantasia;
            selecionarBtnIgnorarTodas();
        }

        else if (acaoSelecionada === 5) {
            $scope.formulario.justificativa = "Câmera " + $scope.slideSelecionado.camera.nomeCamera + " com defeito.  ATENÇÃO Verificar todas as câmeras desse cliente!";
            selecionarBtnAbrirOSAssessoria();

        }
         else if (acaoSelecionada === 6) {
            $scope.formulario.justificativa = "Cliente " + $scope.tarefa.cliente.cdCliente + " DESATIVADO.  ATENÇÃO Verificar todos os termos abertos deste cliente";
            selectionarBtnDesativar();

        }
    };

    $scope.selecionarAcaoClienteComFalha = function (acaoSelecionada) {
        $scope.formulario.acao = acaoSelecionada;
        $scope.showFormClienteComFalha = true;

        if (acaoSelecionada === 1) {
            $scope.formulario.justificativa = "Erros: \n " + $scope.tarefa.descricaoFalha + "\n #OS Aberta";
            selecionarBtnAbrirOS();
        } else if (acaoSelecionada === 2) {
            $scope.formulario.justificativa = "Erros: \n " + $scope.tarefa.descricaoFalha + "\n #Ignorados";
            selectionarBtnIgnorar();
        } else if (acaoSelecionada === 5) {
            $scope.formulario.justificativa = "Erros: \n " + $scope.tarefa.descricaoFalha + "\n #OS ASSESSORIA";
            selecionarBtnAbrirOSAssessoria();
        }
    };


    $scope.atualizarImagemAoVivo = function () {
        var str_data = dia + '/' + (mes+1) + '/' + ano4;
        var str_hora = hora + ':' + min + ':' + seg;
        start();
        limparCameraAoVivo();
        var indexSlideSelecionado = $scope.slides.indexOf($scope.slideSelecionado);
        var indexImagemSelecionada = $scope.tarefa.listImagens.indexOf($scope.slideSelecionado);

        imagensAPI.getImagemAoVivo($scope.tarefa, $scope.slideSelecionado, true)
            .then(function successCallback(response) {
                if (response.data !== null && response.data !== undefined) {
                    if (response.data.pathImagem !== 'erro' && response.data !== "") {
                        $scope.tarefa.listImagens[indexImagemSelecionada] = response.data;
                        $scope.slides[indexSlideSelecionado] = response.data;
                        $scope.slideSelecionado = $scope.slides[indexSlideSelecionado];
                        $scope.labelsImagemCamera = $scope.slideSelecionado.listLabels;
                        $scope.pathImagemCamera = $scope.slideSelecionado.pathImagem;

                    } else if (response.data === "") {

                        setMsgErro("Câmera indisponível!");
                        onInit();
                    } else {
                        onInit();
                         setMsgErro("Erro: D-Guard não está respondendo!");
                    }
                }

                if ($scope.slideSelecionado.listLabels.length < 3) {
                  if ($scope.slideSelecionado.listLabels.length == 0 || $scope.slideSelecionado.listLabels.length == undefined){
                            complete();
                             $scope.formulario.justificativa = "Câmera " + $scope.slideSelecionado.camera.nomeCamera + " com defeito.  ATENÇÃO Verificar todas as câmeras desse cliente!";
                             $scope.selecionarAcao(5);
                              $("#salvarReferencia").prop('disabled', true);
                             $scope.selecionarAcao(5);
                    }else{
                         complete();
                         $scope.formulario.justificativa = "Câmera " + $scope.slideSelecionado.camera.nomeCamera + " com defeito.  ATENÇÃO Verificar todas as câmeras desse cliente!";
                         $scope.selecionarAcao(3);
                         $("#salvarReferencia").prop('disabled', true);
                          $scope.selecionarAcao(3);
                    }
                } else {
                   var k = 0;
                    for (var j = 0; j < $scope.slideSelecionado.listLabels.length; j++) {

                        if ($scope.slideSelecionado.listLabels[j].descricao === "Text" &&  $scope.slideSelecionado.listLabels.length <3) {
                             $scope.selecionarAcao(5);
                              $("#salvarReferencia").prop('disabled', true);
                          
                            complete();
                            break;

                        } else if ($scope.slideSelecionado.listLabels[j].descricao === "Call of Duty" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "The Legend of Zelda" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Grand Theft Auto" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Final Fantasy" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Doom" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Minecraft" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Overwatch" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Gray" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "World Of Warcraft" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Text" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Symbol" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Unreal Tournament" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Smoke" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Fog" 
                            

                        ) {
                            k++;

                        }

                    }

                    var total = j * 0.15;

                    if (k >= total) {
                    $scope.selecionarAcao(3);
                    $("#salvarReferencia").prop('disabled', true);
                   
                    }else{
                         $scope.selecionarAcao(1);
                         $("#salvarReferencia").prop('disabled', false);

                    }
                }

            }, function errorCallback(response) {
                onInit();
                  setMsgErro("Erro: D-Guard não está respondendo!");
                console.log("Erro ao buscar imagem ao vivo. " + response);
                complete();
            });

        $scope.getDadosCentralAlarme();

    };

     $scope.atualizarImagemAoVivoSolo = function () {

        var str_data = dia + '/' + (mes+1) + '/' + ano4;
        var str_hora = hora + ':' + min + ':' + seg;
        start();
        limparCameraAoVivo();
        var indexSlideSelecionado = $scope.slides.indexOf($scope.slideSelecionado);
        var indexImagemSelecionada = $scope.tarefa.listImagens.indexOf($scope.slideSelecionado);

        imagensAPI.getImagemAoVivo($scope.tarefa, $scope.slideSelecionado, true)
            .then(function successCallback(response) {
                if (response.data !== null && response.data !== undefined) {
                    if (response.data.pathImagem !== 'erro' && response.data !== "") {
                        $scope.tarefa.listImagens[indexImagemSelecionada] = response.data;
                        $scope.slides[indexSlideSelecionado] = response.data;
                        $scope.slideSelecionado = $scope.slides[indexSlideSelecionado];
                        $scope.labelsImagemCamera = $scope.slideSelecionado.listLabels;
                        $scope.pathImagemCamera = $scope.slideSelecionado.pathImagem;

                    } else if (response.data === "") {

                        setMsgErro("Câmera indisponível!");
                        onInit();
                    } else {
                        onInit();
                         setMsgErro("Erro: D-Guard não está respondendo!");
                    }
                }

                if ($scope.slideSelecionado.listLabels.length < 3) {
                  if ($scope.slideSelecionado.listLabels.length == 0 || $scope.slideSelecionado.listLabels.length == undefined){
                            complete();
                             $scope.formulario.justificativa = "Câmera " + $scope.slideSelecionado.camera.nomeCamera + " com defeito.  ATENÇÃO Verificar todas as câmeras desse cliente!";
                             $scope.selecionarAcao(5);
                              $("#salvarReferencia").prop('disabled', true);
                             $scope.selecionarAcao(5);
                    }else{
                         complete();
                         $scope.formulario.justificativa = "Câmera " + $scope.slideSelecionado.camera.nomeCamera + " com defeito.  ATENÇÃO Verificar todas as câmeras desse cliente!";
                         $scope.selecionarAcao(3);
                         $("#salvarReferencia").prop('disabled', true);
                          $scope.selecionarAcao(3);
                    }
                } else {
                   var k = 0;
                    for (var j = 0; j < $scope.slideSelecionado.listLabels.length; j++) {

                        if ($scope.slideSelecionado.listLabels[j].descricao === "Text" &&  $scope.slideSelecionado.listLabels.length <3) {
                             $scope.selecionarAcao(5);
                              $("#salvarReferencia").prop('disabled', true);
                          
                            complete();
                            break;

                        } else if ($scope.slideSelecionado.listLabels[j].descricao === "Call of Duty" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "The Legend of Zelda" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Grand Theft Auto" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Final Fantasy" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Doom" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Minecraft" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Overwatch" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Gray" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "World Of Warcraft" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Text" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Symbol" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Unreal Tournament" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Smoke" ||
                            $scope.slideSelecionado.listLabels[j].descricao === "Fog" 
                            

                        ) {
                            k++;

                        }

                    }

                    var total = j * 0.15;

                    if (k >= total) {
                    $scope.selecionarAcao(3);
                    $("#salvarReferencia").prop('disabled', true);
                   
                    }else{

                              if ($scope.labelsref.length > 0 || $scope.slidesReferencias.length>0) {
                                          
                                            if($scope.slidesReferencias.length===0){
                                                  $scope.slidesReferencias.listLabels = $scope.labelsref;  
                                            }

                                            for (var k = 0; k <= $scope.slidesReferencias.length; k++) {
                                                                                                                                          
                                                for (var l = 0; l < $scope.labelsImagemCamera.length; l++) {
                                                 
                                                    if ($scope.labelsImagemReferencia !== null && $scope.labelsImagemReferencia !== undefined) {
                                                        var index = $scope.labelsImagemReferencia.findIndex(label => label.descricao === $scope.labelsImagemCamera[l].descricao);
                                                       
                                                        if (index > -1) {
                                                           
                                                            $scope.labelsImagemReferencia[index].match = true;
                                                            $scope.labelsImagemCamera[l].match = true;
                                                        } else {
                                                           
                                                            $scope.labelsImagemCamera[l].match = false;
                                                        }
                                                    }
                                                    else {
                                                        
                                                        $scope.labelsImagemCamera[l].match = false;
                                                    }
                                                  
                                                }
                                              
                                                $scope.porcentagemMatchImagem = 0;

                                                if ($scope.labelsImagemCamera.length > 0) {
                                                   
                                                    $scope.porcentagemMatchImagem = calcularPorcentagem($scope.labelsImagemCamera);
                                                }
                                               
                                                $scope.porcentagemMatchImagemRef = 0;

                                                if ($scope.labelsImagemReferencia.length > 0) {
                                                
                                                    $scope.porcentagemMatchImagemRef = calcularPorcentagem($scope.labelsImagemReferencia);
                                                }

                                                if ($scope.porcentagemMatchImagemRef >= 20 || $scope.porcentagemMatchImagem >= 20) {
                                                  
                                                    $scope.selecionarAcao(1);
                                                    $scope.salvar();
                                                                                                     
                                                    if ($scope.slides.length !== 0) {
                                                    
                                                    } else {
                                                        complete();
                                                         alert("SUCESSO! imagem atualizada com sucesso e cliente finalizado.");
                                                        $scope.proximaTarefa();
                                                  
                                                    }

                                                } else {
                                                 
                                                }

                                            }
                                            setMsgSucesso("Cliente Atualizado!");

                                        } else {


                                        }
                       
                              
                    }

                    onInit();
                }

            }, function errorCallback(response) {
                onInit();
                  setMsgErro("Erro: D-Guard não está respondendo!");
                console.log("Erro ao buscar imagem ao vivo. " + response);
                complete();
            });

 $scope.getDadosCentralAlarme();

    };


    $scope.reprocessar = function () {
        $('#myModalAguarde').modal({backdrop: 'static', keyboard: false}) 
        
        $('#myModalAguarde').modal('show');
        $('#myModalAguarde').modal({backdrop: 'static', keyboard: false}) 
       
        complete();
        limparCameraAoVivo();
        
        for (var i = 0; i < $scope.slides.length; i++) {
                      
            var indexSlideSelecionado=i;
            var indexImagemSelecionada = i+1;
            $scope.slideSelecionado = $scope.slides[indexSlideSelecionado];
            $scope.labelsref = $scope.slides[indexSlideSelecionado].listLabels;
            
            imagensAPI.getImagemAoVivo($scope.tarefa, $scope.slideSelecionado, true)
                .then(function successCallback(response) {
                    complete();       
                    var indexReferencia = 0;
                    if (response.data !== null && response.data !== undefined) {
                       
                        if (response.data.pathImagem !== 'erro' && response.data !== "") {
                         
                            $scope.tarefa.listImagens[indexImagemSelecionada] = response.data;
                            $scope.slides[indexSlideSelecionado] = response.data;
                            $scope.slideSelecionado = $scope.slides[indexSlideSelecionado];
                            $scope.labelsImagemCamera = $scope.slideSelecionado.listLabels;
                            $scope.pathImagemCamera = $scope.slideSelecionado.pathImagem;
                            $scope.labelsref = $scope.slides[indexSlideSelecionado].listLabels;

                            if ($scope.slideSelecionado.listLabels.length < 3) {
                                  if ($scope.slideSelecionado.listLabels.length == 0 || $scope.slideSelecionado.listLabels.length == undefined){
                                        complete();
                                         $scope.formulario.justificativa = "Câmera " + $scope.slideSelecionado.camera.nomeCamera + " com defeito.  ATENÇÃO Verificar todas as câmeras desse cliente!";
                                         $scope.selecionarAcao(5);
                                         $("#salvarReferencia").prop('disabled', true);
                                         $scope.selecionarAcao(5);
                    }else{
                         complete();
                         $scope.formulario.justificativa = "Câmera " + $scope.slideSelecionado.camera.nomeCamera + " com defeito.  ATENÇÃO Verificar todas as câmeras desse cliente!";
                         $scope.selecionarAcao(3);
                         $("#salvarReferencia").prop('disabled', true);
                          $scope.selecionarAcao(3);
                    }

                            } else {
                                complete();
                                var k = 0;

                            
                                        for (var j = 0; j < $scope.slideSelecionado.listLabels.length; j++) {

                                            if ($scope.slideSelecionado.listLabels[j].descricao === "Text" &&  $scope.slideSelecionado.listLabels.length <3) {
                                                 $scope.selecionarAcao(5);
                                                  $("#salvarReferencia").prop('disabled', true);
                                              
                                                complete();
                                                break;

                                            } else if ($scope.slideSelecionado.listLabels[j].descricao === "Call of Duty" ||
                                                $scope.slideSelecionado.listLabels[j].descricao === "The Legend of Zelda" ||
                                                $scope.slideSelecionado.listLabels[j].descricao === "Grand Theft Auto" ||
                                                $scope.slideSelecionado.listLabels[j].descricao === "Final Fantasy" ||
                                                $scope.slideSelecionado.listLabels[j].descricao === "Doom" ||
                                                $scope.slideSelecionado.listLabels[j].descricao === "Minecraft" ||
                                                $scope.slideSelecionado.listLabels[j].descricao === "Overwatch" ||
                                                $scope.slideSelecionado.listLabels[j].descricao === "Gray" ||
                                                $scope.slideSelecionado.listLabels[j].descricao === "World Of Warcraft" ||
                                                $scope.slideSelecionado.listLabels[j].descricao === "Text" ||
                                                $scope.slideSelecionado.listLabels[j].descricao === "Symbol" ||
                                                $scope.slideSelecionado.listLabels[j].descricao === "Unreal Tournament" ||
                                                $scope.slideSelecionado.listLabels[j].descricao === "Smoke" ||
                                                $scope.slideSelecionado.listLabels[j].descricao === "Fog" 
                                                

                                            ) {
                                                k++;

                                            }

                                        }

                                       
                                                 
                                                               
                                if ($scope.slides.length !== indexReferencia) {
                                    complete();
                                    if ($scope.labelsref.length > 0) {
                                                                             
                                        $scope.labelsImagemReferencia = $scope.labelsref;
                                        indexReferencia++;
                                    }
                                    var total = j * 0.15;
                                  
                                    if (k >= total) {
                                      

                                        $scope.formulario.justificativa = "Imagem câmera " + $scope.slideSelecionado.camera.nomeCamera + " ignorada.";
                                        $("#salvarReferencia").prop('disabled', true);
                                        $scope.selecionarAcao(3);
                                        $("#salvarReferencia").prop('disabled', true);

                                    } else {
                                        complete();                             
                                        if ($scope.labelsref.length > 0 || $scope.slidesReferencias.length>0) {
                                          
                                            if($scope.slidesReferencias.length===0){
                                                  $scope.slidesReferencias.listLabels = $scope.labelsref;  
                                            }

                                            for (var k = 0; k <= $scope.slidesReferencias.length; k++) {
                                                                                                                                          
                                                for (var l = 0; l < $scope.labelsImagemCamera.length; l++) {
                                                 
                                                    if ($scope.labelsImagemReferencia !== null && $scope.labelsImagemReferencia !== undefined) {
                                                        var index = $scope.labelsImagemReferencia.findIndex(label => label.descricao === $scope.labelsImagemCamera[l].descricao);
                                                       
                                                        if (index > -1) {
                                                           
                                                            $scope.labelsImagemReferencia[index].match = true;
                                                            $scope.labelsImagemCamera[l].match = true;
                                                        } else {
                                                           
                                                            $scope.labelsImagemCamera[l].match = false;
                                                        }
                                                    }
                                                    else {
                                                        
                                                        $scope.labelsImagemCamera[l].match = false;
                                                    }
                                                  
                                                }
                                              
                                                $scope.porcentagemMatchImagem = 0;

                                                if ($scope.labelsImagemCamera.length > 0) {
                                                   
                                                    $scope.porcentagemMatchImagem = calcularPorcentagem($scope.labelsImagemCamera);
                                                }
                                               
                                                $scope.porcentagemMatchImagemRef = 0;

                                                if ($scope.labelsImagemReferencia.length > 0) {
                                                
                                                    $scope.porcentagemMatchImagemRef = calcularPorcentagem($scope.labelsImagemReferencia);
                                                }

                                                if ($scope.porcentagemMatchImagemRef >= 20 || $scope.porcentagemMatchImagem >= 20) {
                                                  
                                                    $scope.selecionarAcao(1);
                                                    $scope.salvarReprocessar();
                                                                                                     
                                                    if ($scope.slides.length !== 0) {
                                                    
                                                    } else {
                                                        complete();
                                                        $scope.proximaTarefa();
                                                  
                                                    }

                                                } else {
                                                 
                                                }

                                            }
                                            setMsgSucesso("Cliente Atualizado!");

                                        } else {
                                          
                                            $scope.selecionarAcao(1);
                                            $scope.salvar();
                                          
                                            if ($scope.slides.length !== 0) {
                                             

                                            } else {
                                              
                                                $scope.proximaTarefa();
                                              
                                            }

                                        }
                                        setMsgSucesso("Cliente Atualizado!");
                                    }
                                    $timeout(
                                        function () {
                                            complete();
                                            atualizarElementosReprocessar();
                                           
                                            listarTarefasFiltro();
                                          
                                            getQuantidadeTarefasAbertas();
                                          $scope.qtdOperadores= getQuantidadeOperadoresAtivos();
                                            getInformacaoUsuario();
                                         if ($scope.slides.length > 0) {
                                            complete();

                                            } else {
                                                complete();
                                                $scope.proximaTarefa();
                                              

                                            }
                                        }, 800);
                                   
                                        
                                } else {
                                    complete();
                                    var total = j * 0.15;

                                    if (k >= total) {
                                      
                                        $scope.formulario.justificativa = "Imagem câmera " + $scope.slideSelecionado.camera.nomeCamera + " ignorada.";
                                          $("#salvarReferencia").prop('disabled', true);
                                        $scope.selecionarAcao(3);
                                        $("#salvarReferencia").prop('disabled', true);
                                      
                                    } else {                                  

                                        for (var k = 0; k < $scope.slidesReferencias.length; k++) {
                                           
                                            $scope.labelsImagemReferencia = $scope.slidesReferencias[k].listLabels;

                                            for (var l = 0; l < $scope.labelsImagemCamera.length; l++) {
                                               
                                                if ($scope.labelsImagemReferencia !== null && $scope.labelsImagemReferencia !== undefined) {
                                                    var index = $scope.labelsImagemReferencia.findIndex(label => label.descricao === $scope.labelsImagemCamera[l].descricao);
                                                  
                                                    if (index > -1) {
                                                        $scope.labelsImagemReferencia[index].match = true;
                                                        $scope.labelsImagemCamera[l].match = true;
                                                    } else {
                                                        $scope.labelsImagemCamera[l].match = false;
                                                    }
                                                }
                                                else {
                                                  
                                                    $scope.labelsImagemCamera[l].match = false;
                                                }
                                            }
                                           
                                            $scope.porcentagemMatchImagem = 0;

                                            if ($scope.labelsImagemCamera.length > 0) {
                                               
                                                $scope.porcentagemMatchImagem = calcularPorcentagem($scope.labelsImagemCamera);
                                            }

                                            $scope.porcentagemMatchImagemRef = 0;

                                            if ($scope.labelsImagemReferencia.length > 0) {
                                               
                                                $scope.porcentagemMatchImagemRef = calcularPorcentagem($scope.labelsImagemReferencia);
                                            }

                                            if ($scope.porcentagemMatchImagemRef >= 20 || $scope.porcentagemMatchImagem >= 20) {
                                                complete();
                                                $scope.selecionarAcao(1);
                                                $scope.salvarReprocessar();
                                                complete();
                                                if ($scope.slides.length !== 0) {
                                                   

                                                } else {
                                                    complete();
                                                    $scope.proximaTarefa();
                                              
                                                }

                                            } else {
                                                complete();
                                                $scope.proximaTarefa();
                                              

                                            }

                                        }

                                    }

                                    if ($scope.slides.length > 0) {
                                        complete();
                                        atualizarElementosReprocessar();
                                        
                                        listarTarefasFiltro();
                                       
                                        getQuantidadeTarefasAbertas();
                                       $scope.qtdOperadores= getQuantidadeOperadoresAtivos();
                                        getInformacaoUsuario();

                                    } else {
                                        complete();
                                        $scope.proximaTarefa();
                                     

                                    }
                                 


                                }

                            }
                           
                        } else if (response.data === "") {
                            complete();
                           console.log("Câmera indisponível!");
                        }

                        else {
                            complete();
                           console.log("Erro: D-Guard não está respondendo!");
                        }
                        complete();
                        console.log("Cliente Atualizado!");
                    }

                    indexSlideSelecionado++;
                    indexImagemSelecionada ++;
                }, function errorCallback(response) {
                    complete();
                   onInit();
                // setMsgErro("Erro: D-Guard não está respondendo!");
                    console.log("Erro ao buscar imagem ao vivo. " + response);
                  
                });

          
        }
      
        $timeout(
            function () {

                if ($scope.slides.length > 0) {
                    complete();
                    atualizarElementosReprocessar();
                    listarTarefasFiltro();
                    getQuantidadeTarefasAbertas();
                     $scope.qtdOperadores= getQuantidadeOperadoresAtivos();
                    getInformacaoUsuario();
                    $('#myModalAguarde').modal({backdrop: 'static', keyboard: false})  
                    $('#myModalAguarde').modal('hide');

                    $('#myModalSucesso').modal({backdrop: 'static', keyboard: false})  
                    $('#myModalSucesso').modal('show');
                           $scope.sucessoProximaTarefa();
                    atualizarElementosReprocessar();
                    listarTarefasFiltro();
                    getQuantidadeTarefasAbertas();
                     $scope.qtdOperadores= getQuantidadeOperadoresAtivos();
                    getInformacaoUsuario();

                } else {
                    complete();
                    $scope.proximaTarefa();
                      $('#myModalAguarde').modal({backdrop: 'static', keyboard: false})  
                     $('#myModalAguarde').modal('hide');
                    $('#myModalSucesso').modal({backdrop: 'static', keyboard: false}) 
                    $('#myModalSucesso').modal('show');
                      $('#myModalSucesso').modal({backdrop: 'static', keyboard: false}) 
                    $scope.sucessoProximaTarefa();
                    $scope.proximaTarefa();
                }
               
            }, 12000);
       
    };

    $scope.salvarReprocessar = function () {

        limparCameraAoVivo();
        limparLabelsImagem();
        limparReferencias();

        if ($scope.formulario.acao == 4) {
            ignorarTodasCamerasComDefeito();
            salvarTarefa();
        } else {

            $scope.slideSelecionado.descricaoProblema = null;
            $scope.slideSelecionado.justificativa = $scope.formulario.justificativa;

            if ($scope.formulario.acao === 1) {
                $scope.slideSelecionado.referencia = true;
                $scope.slideSelecionado.tipoTratamento = "Salvar Referência";
                salvarImagem();
               
            }

             else if ($scope.formulario.acao === 2) {
                $scope.slideSelecionado.tipoTratamento = "OS Técnico";
                salvarOSImagemCamera();
              
            } else if ($scope.formulario.acao === 3) {
                $scope.slideSelecionado.tipoTratamento = "Ignorar";
                salvarImagem();
              
            }
            else if ($scope.formulario.acao === 5) {
                $scope.slideSelecionado.tipoTratamento = "OS Assessoria";
                salvarOSImagemCameraAssessoria();
             
            }
            else if ($scope.formulario.acao === 6) {
                $scope.slideSelecionado.tipoTratamento = "Ignorar Cliente";
                updateTesteAutomatico();
             
                
            }

        }
        $scope.getSelectValueProximo();

    };

    $scope.imagemAoVivo = function () {

        start();
        limparCameraAoVivo();
        imagensAPI.getImagemAoVivo($scope.tarefa, $scope.slideSelecionado, false)
            .then(function successCallback(response) {
                start();
                if (response.data !== null && response.data !== undefined) {

                    if (response.data.pathImagem !== 'erro' && response.data !== "") {
                    
                        $scope.imgAoVivo = response.data.pathImagem;
                    } else if (response.data === "") {
                        setMsgErro("Câmera indisponível!");
                    } else {
                        setMsgErro("Erro: D-Guard não está respondendo!");
                    }
                }

                  
                

                complete();

            }, function errorCallback(response) {
                setMsgErro("Erro: D-Guard não está respondendo!");
                console.log("Erro ao buscar imagem ao vivo. " + response);
                complete();
            });

            $scope.getDadosCentralAlarme();

    };

    $scope.getDetalhesOS = function (index) {
        $scope.indexOsSelecionado = index;
      
       $('#myModalDetalheOs').modal('show');
       
    }

     $scope.detalhesEmail = function (indexRat) {
    	$scope.indexRat = indexRat;

     $('#myModalAguarde2').modal('show');
       
    }

    $scope.detalhesOperadores = function () {
         $scope.operadoresAtivos;

       for (var i = 0; i < $scope.operadoresAtivos.length; i++) {
            $scope.formated = $scope.formated + $scope.operadoresAtivos[i];
       }
       var letra = $scope.formated.split("\n");
        letra.sort();
        $scope.formated = letra.join("\n");
        alert("Lista de usuários Ativos no momento:\n"+$scope.formated);
      
       // $('#myModalDetalhesOperadores').modal('show');
       
    }
    

    $scope.sucessoProximaTarefa = function () {

        setTimeout(function () {
            $('#myModalSucesso').modal('hide');
            onInit();
        }, 7000);

    };

   

    $scope.aplicaFiltroCheckboxRat = function () {
        $scope.checkedEnviaRat = !$scope.checkedEnviaRat;
        console.log("aplicaFiltroCheckbox");
       
    };

     $scope.aplicaFiltroCheckbox = function () {
        $scope.checkedSomenteCamerasDefeito = !$scope.checkedSomenteCamerasDefeito;
        console.log("aplicaFiltroCheckbox");
        filtrarCheckbox();
    };

    $scope.aplicaFiltroCheckboxServidorDefeito = function () {
        $scope.checkboxErroServidor = !$scope.checkboxErroServidor;
        console.log("checkboxErroServidor");
        filtrarCheckboxServidor();
    };

    $scope.proximaTarefa = function () {
        $scope.showFormClienteComFalha = false;
        limparReferencias();

         try {
            var idTarefaAtual = $scope.tarefa.id !== undefined ? $scope.tarefa.id : 0;

             if($scope.selectedValue==1){
                 
                  getProximaTarefa(idTarefaAtual, $rootScope.operador);
                  getHistoricoOS();
                  getHistoricoTarefas();
                  getRatsEnviadas();

                }else {
                    $scope.proximaTarefaFiltrada();

                }
                 $("#rat").prop('disabled', false);
            
        
        // onInit();
        }
        catch (e) {
         // $scope.proximaTarefa();
        }

        

       
      
       
    };

      $scope.proximaTarefaFiltro = function () {
        $scope.showFormClienteComFalha = false;
        limparReferencias();

         try {
            var idTarefaAtual = $scope.tarefa.id !== undefined ? $scope.tarefa.id : 0;
            
                    $scope.proximaTarefaFiltrada();
                    $("#rat").prop('disabled', false);
        
        // onInit();
        }
        catch (e) {
         // $scope.proximaTarefa();
        }

        

       
      
       
    };

$scope.proximaTarefaFiltrada = function () {
        $scope.showFormClienteComFalha = false;
        limparReferencias();

        try {
         var idTarefaAtual = $scope.tarefa.id !== undefined ? $scope.tarefa.id : 0;
          getProximaTarefaFiltrada(idTarefaAtual, $rootScope.operador,$scope.selectedValue);
           getHistoricoOS();
           getHistoricoTarefas();
           getRatsEnviadas();
        // onInit();
        }
        catch (e) {
         // $scope.proximaTarefa();
        }
      
       
    };

    $scope.tarefaAnterior = function () {

        if ($scope.tarefasMemoria !== undefined && $scope.tarefasMemoria.length > 0) {
            $scope.showFormClienteComFalha = false;
            limparReferencias();

            var indexTarefa = $scope.tarefasMemoria.indexOf($scope.tarefa.id);
            if (indexTarefa > 0) {
                $scope.tarefasMemoria.pop(indexTarefa);
                getTarefaAnterior($scope.tarefasMemoria[indexTarefa - 1], $scope.tarefa.id, $rootScope.operador);
            } else {
                getProximaTarefa($scope.tarefa.id, $rootScope.operador);
            }
        }
        onInit();
    };

// criar um select em todos os clientes e substituir este pelo all
    $scope.autoCompleteFiltro = function (str, tarefasFiltro) {
        var matches = [];
        $scope.tarefasFiltro.forEach(function (tarefa) {
            if (tarefa !== undefined) {
                if (('' + tarefa.cliente.cdCliente).toLowerCase().indexOf(str.toString().toLowerCase()) >= 0 ||
                    (('' + tarefa.cliente.razaoSocial).toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) ||
                    (('' + tarefa.cliente.fantasia).toLowerCase().indexOf(str.toString().toLowerCase()) >= 0)) {
                    matches.push(tarefa);
                }
            }
        });

        return matches;
    };

    $scope.autoCompleteFiltroAll = function (str, tarefasFiltroAll) {
        var matches = [];
        $scope.tarefasFiltroAll.forEach(function (tarefa) {
            if (tarefa !== undefined) {
                if (('' + tarefa.cliente.cdCliente).toLowerCase().indexOf(str.toString().toLowerCase()) >= 0 ||
                    (('' + tarefa.cliente.razaoSocial).toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) ||
                    (('' + tarefa.cliente.fantasia).toLowerCase().indexOf(str.toString().toLowerCase()) >= 0)) {
                    matches.push(tarefa);
                }
            }
        });

        return matches;
    };

    $scope.onCarouselBeforeChange = function (currentSlide) {
      
        limparLabelsImagem();
        fecharInfoCliente();
        limparCameraAoVivo();
        limparReferencias();
        limparLabelsImagem();
    };

    $scope.onCarouselAfterChange = function (currentSlide) {
        
        try{
            $scope.labelsImagemCamera = $scope.slides[currentSlide].listLabels;
            $scope.slideSelecionado = $scope.slides[currentSlide];
            filtrarCheckbox();
        }catch{

          $scope.proximaTarefa();
          $scope.encerrarTarefa();

        }
       
    };

    $scope.onCarouselRefAfterChange = function (currentSlide) {
      
        $scope.labelsImagemReferencia = $scope.slidesReferencias[currentSlide].listLabels;
        pintarLabels();
    };

    $scope.onCarouselInit = function () {
        draggableModal();
    };

    $scope.salvar = function () {

        limparCameraAoVivo();
        limparLabelsImagem();
        limparReferencias();

        if ($scope.formulario.acao == 4) {
            ignorarTodasCamerasComDefeito();
            salvarTarefa();
        } else {

            $scope.slideSelecionado.descricaoProblema = null;
            $scope.slideSelecionado.justificativa = $scope.formulario.justificativa;

            if ($scope.formulario.acao === 1 &&  $scope.totalReferenciasCurrent<5) {
                $scope.slideSelecionado.referencia = true;
                $scope.slideSelecionado.tipoTratamento = "Salvar Referência";
                salvarImagem();
               
            }else if ($scope.formulario.acao === 1 &&  $scope.totalReferenciasCurrent>=5) {
                alert("ATENÇÃO! Já existem 5 Referências para esta câmera. Favor, Validar as referências e subistituir caso necessário.");
                
                buscarReferencias($scope.slideSelecionado.camera);

            }

             else if ($scope.formulario.acao === 2) {
                $scope.slideSelecionado.tipoTratamento = "OS Técnico";
                salvarOSImagemCamera();
              
            } else if ($scope.formulario.acao === 3) {
                $scope.slideSelecionado.tipoTratamento = "Ignorar";
                salvarImagem();
              
            }
            else if ($scope.formulario.acao === 5) {
                $scope.slideSelecionado.tipoTratamento = "OS Assessoria";
                salvarOSImagemCameraAssessoria();
             
            }
            else if ($scope.formulario.acao === 6) {
                $scope.slideSelecionado.tipoTratamento = "Ignorar Cliente";
                updateTesteAutomatico();
             
                
            }

        }
        $scope.getSelectValueProximo();

    };

    var allowedKeys = {
             37: 'left',
             38: 'up',
             39: 'right',
             40: 'down',
             65: 'a',
             66: 'b'
};
// the 'official' Konami Code sequence
var konamiCode = ['up', 'up', 'down', 'down', 'left', 'right', 'left', 'right', 'b', 'a'];
// a variable to remember the 'position' the user has reached so far.
var konamiCodePosition = 0;
// add keydown event listener
document.addEventListener('keydown', function(e) {
 // get the value of the key code from the key map
 var key = allowedKeys[e.keyCode];
 // get the value of the required key from the konami code
 var requiredKey = konamiCode[konamiCodePosition];
 // compare the key with the required key
 if (key == requiredKey) {
   // move to the next key in the konami code sequence
         konamiCodePosition++;
   // if the last key is reached, activate cheats
   if (konamiCodePosition == konamiCode.length) {
         activateCheats();
         konamiCodePosition = 0;
   }
 } else {
           konamiCodePosition = 0;
 }
    
    });


function activateCheats() {

 alert("Existem "+  $scope.qtdOperadores+ " ativos no momento.");
}

    $scope.salvarClienteComFalha = function () {

        if ($scope.formulario.acao === 1) {
            salvarOSClienteComFalha();
        } else if ($scope.formulario.acao === 2) {
            encerrarTarefaComFalha();
        }
        else if ($scope.formulario.acao === 5) {
            encerrarTarefaComFalhaAssessoria();
        }
    };

    onInit();
   

});