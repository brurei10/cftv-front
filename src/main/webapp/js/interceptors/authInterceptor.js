'use strict';

angular.module('cftvInteligente').factory('authInterceptor', function($q, $cookies, LOGOUT_URL) {
    return {
        request: function(config) {
            config.headers = config.headers || {};
            //if ($cookies.get('token') && $cookies.get('token') != "null") {
            config.headers['X-AUTH-TOKEN'] = $cookies.get('token');
            //}
            return config;
        },
        response: function(res) {
            if (res.status === 200) {
                if (res.config.url.indexOf("/ping") > -1) { // renew
                    $cookies.put('token', res.headers('X-AUTH-TOKEN'));
                }
                if (!$cookies.get('token') || $cookies.get('token') == "null" || $cookies.get('token') == null) {
                    $cookies.put('token', res.headers('X-AUTH-TOKEN'));
                }
            }
            return res || $q.when(res);
        },
        responseError: function(rejection) {

            if (rejection.status === 401) { // usuário não autenticado
                console.log('deslogar do sistema');
                $cookies.put('token', "null");
                location.href = LOGOUT_URL;


            }

            return $q.reject(rejection);

        }
    };
});