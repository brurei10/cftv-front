'use strict';

angular.module('cftvInteligente').factory('errorHandlerInterceptor', function($q, $rootScope){
  return{

    request: function(config) {
      return config;
    },
    requestError : function(rejection) {
      return rejection;
    },
    responseError : function(rejection) {

      if( rejection.status === 401 ) { // usuário não autenticado
        //growl.error('Usuário não autenticado.');
        $rootScope.$broadcast("ErrorInterceptor", 401);
        return $q.reject(rejection);
      }

      if( rejection.status === 400 ) { // é um erro tratado pela aplicação.
        //growl.warning(rejection.data);
        $rootScope.$broadcast("ErrorInterceptor", 400);
        return $q.reject(rejection);
      }

      if( rejection.status === 404 ) { // recurso indisponível ou consulta sem resultado.
        //growl.warning('Você tentou acessar um recurso indisponível.');
        $rootScope.$broadcast("ErrorInterceptor", 404);
        return $q.reject(rejection);
      }

      if( rejection.status >= 500) { // é um erro desconhecido
        //growl.error('Erro desconhecido.');
        $rootScope.$broadcast("ErrorInterceptor", 500);
        return $q.reject(rejection);
      }

       return $q.reject(rejection);

    }
  };
});
