'use strict';

angular.module('cftvInteligente').factory('hostAddressInterceptor', function($q, BACKEND){
    return{
        request: function(config) {
        	
        	var contains = function(value) {
				return config.url.indexOf(value) > -1;
        	};	

            if(  contains('html') ) {
                return config;
            }
            console.log('teste');
            console.log(config.url);
            config.url = BACKEND + config.url;

            return config;

        }
    };
});
