# CFTV Front-end

[![N|Solid](https://i.ibb.co/5BDqdCV/CFTV-fluxo-de-neg-cio.png)](https://i.ibb.co/5BDqdCV/CFTV-fluxo-de-neg-cio.png)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Projeto destinado à validação e tratativas de imagens pelos operadores. Casa cliente possui uma gama de câmeras, as quais, algumas das vezes, acabam ficando fora do ar, ou estão com algum tipo de problema nas imagens.
Este projeto, ter por objetivo, efetuar testes rotineiros nestas câmeras (executados pelo módulo [processamento](https://bitbucket.org/cftv-processamento/src/master/)). Caso, as condicionais de uma "câmera ok"não sejam atendidas,
estas são jogadas para tratamento manual pelo operador em tela (tarefa aberta).

Este Front, tem por função, disponibilizar para o usuário uma interface gráfica amigável que seja possível o tratamento e visualização das tarefas.

  - Serviços
  - Consultas e atualização em bases do SIGMA
  - Consultas ao D-Guard
  - Reprocesssamentos manuais

# Features!

  - Visualização dos clientes e câmeras com defeito;
  - Filtro por cliente;
  - Filtros por tipo de erro que hoje são: Labels não Satisfazem, Erro de conexão com o servidor,Câmeras sem Referência e Não foi possível obter a imagem.
  - Filtrar apenas câmeras com defeito em tela;
  - Reprocessamento;
  - Atualizar imagem ao vivo;
  - Verificar imagem ao vivo;
  - Salvar Referência;
  - Abrir Os para Técnico;
  - Abrir Os para Assessoria;
  - Ignorar imagem;
  - Ignorar Todas;
  - Inserir Termo;
  - Inserir justificativas;
  - Excluir uma ou mais referências;
  - Dados do cliente;
  - Status de alteração;
  - Operadores Online;

# Você precisa saber

  - Os projetos relacionados são
  - /cftv-back/
  - /cftv-processamento/
  - Regenciado e controlado pela pipeline jenkins:
  - /view/CFTV/
  - Envia emails ao iniciar e finalizar o processamento.

Para mais informações e documentações sobre o projeto, basta acessar o  [Sharepoint]

### Especificações Técnicas:

Para desenvolver neste sistema você precisará:

* [AngularJS] - Gerenciador e estrutura html do cftv-front!
* [Eclipse Oxigen](https://www.eclipse.org/downloads/packages/release/oxygen) - IDE de desenvolvimento deste projeto.
* [Spring](https://spring.io/guides/gs/spring-boot/) - Projeto desenvolvido e implantado com spring boot
* [node.js] - I/O do backend
* [Maven] - Gerenciador de dependências do projeto
* [REST] - Para trânsito de serviços e end-points
* [Java 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) - Versão atualizada para o projeto
* [Mysql] - Banco de dados relacional utilizado no SGBD
* [GIT] - Controle de versão

### Instalação

Requer o Node na sua máquina [Node.js](https://nodejs.org/) v4+ para rodar.

Instale as dependências do server e rode.

```sh
$ cd dillinger
$ npm install -d
$ node app
```

Para os demais packs...

```sh
$ npm install --production
$ NODE_ENV=production node app
```

### Plugins

Alguns dos plugins necessários para rodar todos os módulos do CFTV

| Plugin | README |
| ------ | ------ |
| Maven | [https://maven.apache.org/][PlDb] |
| Git | [https://git-scm.com/][PlGh] |
| Spring | [https://spring.io/guides/gs/spring-boot/][PlGd] |

### Todos

Licença
----



**Até a Próxima e bons códigos**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [markdown-it]: <https://github.com/markdown-it/markdown-it>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>
